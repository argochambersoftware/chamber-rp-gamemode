# Chamber RP
### Argochamber interactive 2016

* * *

## Preamble

**DISCLAIMER:** you may not notify errors to the developers via personal mail, use the ticket system.

Before modifying the software, remember that the support is **only** applied to the _original software_.
Modifying the software results in a **void** _support_.

The error reporting must be clear, beware!

* * *

## Chamber RP
##### An Argochamber interactive's GMod Roleplay Gamemode

###### Version 1.0

Base gamemode

Module | Completed
-------|----------
Inventory system | 25%
Module Loader and Environment (_Core_) | 45%
Private property | 75%
Money | 35%
Jobs _Version 1.0_ | 85%


Environment framework example:
```lua
-- Environment for the gamemode
-- Using namespace chamber
Chamber.setVar("lol", true);

local lol = Chamber.getVar("lol"); --true
-- ...
```

* * *

##### Modules

Sample header for a module:
```Lua
--[[
	Sample module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Sample";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "The purpose of this module is just to serve as a placeholder and example.";
```

###### Money Module
###### - [x] Displays money DEPRECATED
######	- [x] Displays money in a nice way DEPRECATED
- [x] Player money in a Networked Value
- [ ] Player money uses DDBB

###### Inventory Module
- [x] Show Up inventory GUI
	- [ ] Drag and drop items
	- [ ] Weight/volume reporting (Optional)
	- [ ] Using values from a real inventory
- [ ] Inventory uses DDBB

###### Core Module
- [x] Environment variables for client/server
- [x] Networked global variables for client/server
- [x] Class loader
- [x] Module loader
- [ ] Commands
- [ ] Chat tweaks
- [ ] Team handling
- [ ] Scoreboard
- [ ] Auth Rights system
- [ ] MOTD
- [x] Player's HUD
- [ ] Player's stats
- [ ] Events system
	- [ ] Event handling
- [ ] Dangling player handle