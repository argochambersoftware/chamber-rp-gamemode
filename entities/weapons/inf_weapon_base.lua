AddCSLuaFile()
--/**
-- * Infinite weapon base
-- */

DEFINE_BASECLASS("inf_sck_base")

TYPE_BULLET				= 1
TYPE_PROJECTILE 		= 2
TYPE_BEAM				= 3

SWEP.PrintName			= "Infinite Base Weapon"
SWEP.Author				= "Argochamber"
SWEP.Spawnable			= false

SWEP.ViewModel 			= "models/weapons/v_pistol.mdl"
SWEP.WorldModel 		= "models/weapons/w_pistol.mdl"
SWEP.HoldType 			= "pistol"
SWEP.ShowViewModel 		= true
SWEP.ShowWorldModel 	= true

--/** Must-Override statements **/--

--Ever override!
SWEP.electromagnetic 	= 0
SWEP.kinetic 			= 0
SWEP.thermal 			= 0
SWEP.radiation 			= 0
--Those 4 Statemens calculate the DPSecond or the DPShot

SWEP.type				= TYPE_BULLET

--If need, upon type provided, for each type, there are many statements that are need, and are all diferent.

--So provided that:
SWEP.knockback 			= 0 --For bullets and beams

SWEP.projectilesPerShot = 1 --For bullets only
SWEP.spread				= 1 --For bullets only

SWEP.delay				= 0.1 -- Delay in seconds to fire again bullets and projectiles
SWEP.recoil				= 0.1 -- Recoil bullets and projectiles

SWEP.chargeDelay		= 0 --If the gun doesn't need to charge the firing, leave-it 0

SWEP.shootSound			= NULL --Sound emitted when firing
SWEP.chargeSound		= NULL --Only needed if the weapon makes a pre-fire sound

SWEP.muzzleEffect		= NULL

SWEP.mods = {} --This can be modified dinamically

--Internal

SWEP.canShoot = true
SWEP.shooting = false

SWEP.lastShoot = 0

function SWEP:addMod(mod)
	table.insert(self.mods, mod)
end

function SWEP:applyMods(moddedObject)
	for k, mod in pairs(self.mods) do
		mod:applyModifiers(moddedObject)
	end
end

function SWEP:PreInit()
	
end

function SWEP:getDamage()
	return Damage(self.electromagnetic, self.kinetic, self.thermal, self.radiation)
end
/*
function SWEP:ShootEffects()
	if SERVER then
		self:SendWeaponAnim( ACT_VM_IDLE )
		self:SendWeaponAnim( ACT_VM_PRIMARYATTACK ) --View model animation 
		
		if self.muzzleEffect == NULL or self.muzzleEffect == nil then
			self.Owner:MuzzleFlash() --Crappy muzzle light, default use 
		else
			
		end
		self.Owner:SetAnimation( PLAYER_ATTACK1 ) --3rd person animation
	end
end
*/
function SWEP:PrimaryAttack()

	if self.canShoot then
		
		if self.chargeDelay > 0 and not self.shooting then
			self.shooting = true
			
			timer.Create("inf_weapon_charge_"..self.PrintName..":"..self.Owner:SteamID(), self.chargeDelay, 1, function()
				self.Shoot(self)
			end)
		elseif not self.shooting then
			
			self:Shoot()
		end
		
	end
	
	
end

function SWEP:enableShoot()
	if SERVER then
		self.canShoot = true
		self.Weapon:SetNWBool( "canShoot", self.canShoot )
	end
end

function SWEP:Shoot(selfEnt)

	--Copies the SWEP info so it can be modded
	local moddedWeapon = MetaSWEP(self)
	self:applyMods(moddedWeapon)
	
	
	if SERVER then
		self.shooting = false
		
		self:ShootEffects()
		
		if self.type == TYPE_BULLET then
			
			local o = self.Owner:GetEyeTrace()
			local bullet =  {
				Attacker = self.Owner,
				Callback = function(ply, tr, dmginfo)
					local e = tr.Entity
					applyDirectDamage( e, moddedWeapon:getDamage(), self )
				end ,
				Damage = 0,
				Force = moddedWeapon.knockback,
				Num = moddedWeapon.projectilesPerShot,
				Spread = Vector(moddedWeapon.spread/360, moddedWeapon.spread/360, 0),
				Src = o.StartPos,
				Dir = o.Normal
			}
			
			self.Weapon:FireBullets(bullet)
		
		elseif self.type == TYPE_PROJECTILE then
			
		elseif self.type == TYPE_BEAM then
			
		end
		
		if self.delay > 0 then
			self.canShoot = false
			self.Weapon:SetNWBool( "canShoot", self.canShoot )
			if CurTime() > self.lastShoot+self.delay then
				self:enableShoot()
			end
		end
	end
	if CLIENT then
		if self.Weapon:GetNWBool( "canShoot" ) then
			local o = self.Owner:GetEyeTrace()
			local bullet =  {
				Attacker = self.Owner,
				Damage = 0,
				Force = 0,
				Num = moddedWeapon.projectilesPerShot,
				Spread = Vector(moddedWeapon.spread/360, moddedWeapon.spread/360, 0),
				Src = o.StartPos,
				Dir = o.Normal
			}
			self.Weapon:FireBullets(bullet)
		end
	end
end

function SWEP:SecondaryAttack()
	
end

