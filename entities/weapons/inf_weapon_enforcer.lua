AddCSLuaFile()
--/**
-- * Infinite weapon base
-- */

DEFINE_BASECLASS("inf_weapon_base")



SWEP.PrintName			= "Enforcer"
SWEP.Instructions		= "Single shot 9mm Gauss Sabot type gun"
SWEP.Category			= "Infinite"
SWEP.Author				= "Argochamber"
SWEP.Spawnable			= false

SWEP.electromagnetic 	= 0
SWEP.kinetic 			= 12
SWEP.thermal 			= 3
SWEP.radiation 			= 0

SWEP.type				= TYPE_BULLET

SWEP.knockback 			= 1

SWEP.projectilesPerShot = 1
SWEP.spread				= 16

SWEP.delay				= 0.5

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_pistol.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.hammer"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["ValveBiped.square"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}
SWEP.VElements = {
	["RetroMass"] = { type = "Model", model = "models/items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.square", rel = "", pos = Vector(0, 0.518, -0.519), angle = Angle(78.311, 92.337, 0), size = Vector(0.5, 0.69, 0.237), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["coiling1"] = { type = "Model", model = "models/items/battery.mdl", bone = "ValveBiped.base", rel = "Shaft", pos = Vector(0, 0, 0.3), angle = Angle(90, 0, -90), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["coiling2"] = { type = "Model", model = "models/items/battery.mdl", bone = "ValveBiped.base", rel = "Shaft", pos = Vector(0.1, 0, 0), angle = Angle(0, 0, -90), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["Shaft"] = { type = "Model", model = "models/maxofs2d/hover_classic.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0, -1.558, 0), angle = Angle(0, 0, 90), size = Vector(0.172, 0.172, 0.172), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["coiling3"] = { type = "Model", model = "models/items/battery.mdl", bone = "ValveBiped.base", rel = "Shaft", pos = Vector(0, 0, -0.301), angle = Angle(-90, 0, -90), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
