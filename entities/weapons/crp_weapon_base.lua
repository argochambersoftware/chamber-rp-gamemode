AddCSLuaFile()

if CLIENT then

    SWEP.DrawAmmo           = true
    SWEP.DrawCrosshair      = false
    SWEP.ViewModelFOV       = 55
    SWEP.ViewModelFlip      = false
	
end

SWEP.Base = "weapon_base"

SWEP.Author = "Argochamber Interactive"
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""

SWEP.Category = "Chamber RP - Weaponry"

SWEP.Spawnable = false
SWEP.UseHands = true

SWEP.HoldType = "normal"

SWEP.Primary.Sound = Sound("Weapon_Pistol.Single")
SWEP.Primary.Recoil = 1
SWEP.Primary.Damage = 40
SWEP.Primary.NumShots = 1
SWEP.Primary.Cone = 1
SWEP.Primary.Delay = 1

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.NormalPos = Vector(0,0,0)
SWEP.NormalAng = Angle(0,0,0)

function SWEP:Initialize()

	self:SetNWInt("mode",0) -- 0 = normal, 1 = aiming

end

function SWEP:Deploy()

	
end

function SWEP:Holster()


end

function SWEP:Reload()
	
end

/*---------------------------------------------------------
PrimaryAttack
---------------------------------------------------------*/
function SWEP:ShootBullet( damage, num_bullets, aimcone )

	local bullet = {}

	bullet.Num 	= num_bullets
	bullet.Src 	= self.Owner:GetShootPos() -- Source
	bullet.Dir 	= self.Owner:GetAimVector() -- Dir of bullet
	bullet.Spread 	= Vector( aimcone, aimcone, 0 )	-- Aim Cone
	bullet.Tracer	= 5 -- Show a tracer on every x bullets
	bullet.Force	= 1 -- Amount of force to give to phys objects
	bullet.Damage	= damage
	bullet.AmmoType = "Pistol"

	self.Owner:FireBullets( bullet )

	self:ShootEffects()
	self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	self.Owner:SetAnimation(PLAYER_ATTACK1)

end

function SWEP:PrimaryAttack()
  
end

function SWEP:SecondaryAttack()
end

local IRONSIGHT_TIME = 0.2

if CLIENT then

/*---------------------------------------------------------
Name: GetViewModelPosition
Desc: Allows you to re-position the view model
---------------------------------------------------------*/
function SWEP:GetViewModelPosition(pos, ang)
    if not self.IronSightsPos then return pos, ang end
	
	local newpos = self.NormalPos
	
    local bIron = false
	
	if self:GetNWInt("mode") == 1 then
	
		bIron = true
	
	else
	
		bIron = false
	
	end

    if bIron ~= self.bLastIron then
        self.bLastIron = bIron
        self.fIronTime = CurTime()

        if bIron then
            self.SwayScale  = 0.3
            self.BobScale   = 0.1
        else
            self.SwayScale  = 1.0
            self.BobScale   = 1.0
        end
    end

    local fIronTime = self.fIronTime or 0

	local Right     = ang:Right()
    local Up        = ang:Up()
    local Forward   = ang:Forward()

    pos = pos + newpos.x * Right
    pos = pos + newpos.y * Forward
    pos = pos + newpos.z * Up

    if not bIron and fIronTime < CurTime() - IRONSIGHT_TIME then
        return pos, ang
    end

    local Mul = 1.0

    if fIronTime > CurTime() - IRONSIGHT_TIME then
        Mul = math.Clamp((CurTime() - fIronTime) / IRONSIGHT_TIME, 0, 1)

        if not bIron then Mul = 1 - Mul end
    end

    local Offset    = self.IronSightsPos

    if self.IronSightsAng then
        ang = ang * 1
        ang:RotateAroundAxis(ang:Right(),   self.IronSightsAng.x * Mul)
        ang:RotateAroundAxis(ang:Up(),      self.IronSightsAng.y * Mul)
        ang:RotateAroundAxis(ang:Forward(), self.IronSightsAng.z * Mul)
    end

    local Right     = ang:Right()
    local Up        = ang:Up()
    local Forward   = ang:Forward()

    pos = (pos + (Offset.x - newpos.x) * Right * Mul)
    pos = (pos + (Offset.y - newpos.y) * Forward * Mul)
    pos = (pos + (Offset.z - newpos.z) * Up * Mul)

    if not self.hasShot then
        if self.IronSightsAngAfterShootingAdjustment then
            ang:RotateAroundAxis(ang:Right(),   self.IronSightsAngAfterShootingAdjustment.x * Mul)
            ang:RotateAroundAxis(ang:Up(),      self.IronSightsAngAfterShootingAdjustment.y * Mul)
            ang:RotateAroundAxis(ang:Forward(), self.IronSightsAngAfterShootingAdjustment.z * Mul)
        end

        if self.IronSightsPosAfterShootingAdjustment then
            Offset = self.IronSightsPosAfterShootingAdjustment
            Right = ang:Right()
            Up = ang:Up()
            Forward = ang:Forward()

            pos = pos + Offset.x * Right * Mul
            pos = pos + Offset.y * Forward * Mul
            pos = pos + Offset.z * Up * Mul
        end
    end

    return pos, ang
end

end

if SERVER then

	function SWEP:Think()

		if self.Owner:KeyDown(IN_ATTACK2) then
			
			if self:GetNWInt("mode") != 1 then
			
				self.Owner:SetFOV(60,0.5)
				self:SetNWInt("mode",1)
			
			end
	   
		elseif !self.Owner:KeyDown(IN_ATTACK2) then
		
			if self:GetNWInt("mode") != 0 then
			
				self.Owner:SetFOV(0,0.1)
				self:SetNWInt("mode",0)
			
			end
		
		end

	end
	
end
