AddCSLuaFile()

SWEP.Base = "crp_weapon_base"

SWEP.Primary.Sound = Sound("Weapon_Pistol.Single")
SWEP.Primary.Recoil = 1.3
SWEP.Primary.Damage = 20
SWEP.Primary.NumShots = 1
SWEP.Primary.Cone = 0.05
SWEP.Primary.Delay = 0.08

SWEP.Primary.ClipSize = 30
SWEP.Primary.DefaultClip = 30
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"

SWEP.ViewModel = "models/weapons/cstrike/c_rif_m4a1.mdl"
SWEP.WorldModel = "models/weapons/w_rif_m4a1.mdl"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.NormalPos = Vector(-2, 0, -0.65)

SWEP.IronSightsPos = Vector(-7.83, -3, 0.259)
SWEP.IronSightsAng = Vector(3, -1.4, -3)