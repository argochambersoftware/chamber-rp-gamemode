AddCSLuaFile()

if CLIENT then
	SWEP.PrintName = "KEYS"
	SWEP.Slot = 1
	SWEP.SlotPos = 3
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end

SWEP.Base = "weapon_base"

SWEP.Author = "Argochamber Interactive"
SWEP.Category = "Argochamber Roleplay - Utility"
SWEP.Instructions = "PRIMARY FIRE to lock. \nSECONDARY FIRE to unlock.\nRELOAD to open PROPERTY MENU"
SWEP.Contact = ""
SWEP.Purpose = "Lock/Unlock owned doors.\nEnter properties menu."

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix = "passive"
SWEP.HoldType = "passive"

SWEP.Spawnable = false
SWEP.AdminSpawnable = true

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""


function SWEP:Initialize()
	if (SERVER) then
		self:SetWeaponHoldType(self.HoldType)
	end
end

function SWEP:DrawWorldModel()
end



function SWEP:Deploy()

	self.Owner:DrawViewModel( false ) 

end

function SWEP:PrimaryAttack()

	if CLIENT then return end

	local ent
	
	if self.Owner:GetEyeTrace().Hit then
	
		ent = self.Owner:GetEyeTrace().Entity
	
		if ent:GetClass() == "prop_door_rotating" or ent:GetClass() == "prop_door" or ent:GetClass() == "func_door" or ent:GetClass() == "func_door_rotating" then
		
			for k,v in pairs(ents.GetAll()) do
			
				if v:GetClass() == "property" then
				
					if v.Class.UID == ent.UID then
					
						if v.Class.owner == self.Owner or table.HasValue(v.Class.coowners, self.Owner) then
						
							ent:Fire("lock","",0)
						
						end
					
					end
				
				end
			
			end
			
		end
		
	end
	
end

function SWEP:SecondaryAttack()
	if CLIENT then return end

	local ent
	
	if self.Owner:GetEyeTrace().Hit then
	
		ent = self.Owner:GetEyeTrace().Entity
	
		if ent:GetClass() == "prop_door_rotating" or ent:GetClass() == "prop_door" or ent:GetClass() == "func_door" or ent:GetClass() == "func_door_rotating" then
		
			for k,v in pairs(ents.GetAll()) do
			
				if v:GetClass() == "property" then
				
					if v.Class.UID == ent.UID then
					
						if v.Class.owner == self.Owner or table.HasValue(v.Class.coowners, self.Owner) then
						
							ent:Fire("unlock","",0)
						
						end
					
					end
				
				end
			
			end
			
		end
		
	end
	
end