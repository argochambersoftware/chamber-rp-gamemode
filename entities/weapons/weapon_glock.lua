AddCSLuaFile()

SWEP.Base = "crp_weapon_base"

SWEP.Primary.Sound = Sound("Weapon_Pistol.Single")
SWEP.Primary.Recoil = 0.8
SWEP.Primary.Damage = 20
SWEP.Primary.NumShots = 1
SWEP.Primary.Cone = 0.08
SWEP.Primary.Delay = 0.15

SWEP.Primary.ClipSize = 20
SWEP.Primary.DefaultClip = 20
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "pistol"

SWEP.ViewModel = "models/weapons/cstrike/c_pist_glock18.mdl"
SWEP.WorldModel = "models/weapons/w_pist_glock18.mdl"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.NormalPos = Vector(-4, 0, 0.239)

SWEP.IronSightsPos = Vector(-5.781, 0, 2.599)
SWEP.IronSightsAng = Vector(0.85, 0, 0)