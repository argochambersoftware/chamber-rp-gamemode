function EFFECT:Init( data )
	self.particles = 4
	self.Data = data
end

function EFFECT:Think()
	return false
end

function EFFECT:Render()
	local data = self.Data
	local vOffset = data:GetOrigin() + Vector( 0, 0, 0.2 )
	local vAngle = data:GetAngles()
	
	local emitter = ParticleEmitter( vOffset, false )
		for i=0, self.particles do
			local particle = emitter:Add( "effects/softglow", vOffset )
			if particle then
				particle:SetAngles( vAngle )
				particle:SetVelocity( Vector( 0, 0, 15 ) )
				particle:SetColor( 255, 102, 0 )
				particle:SetLifeTime( 0 )
				particle:SetDieTime( 0.2 )
				particle:SetStartAlpha( 255 )
				particle:SetEndAlpha( 0 )
				particle:SetStartSize( 1.6 )
				particle:SetStartLength( 1 )
				particle:SetEndSize( 1.2 )
				particle:SetEndLength( 4 )
			end
		end
	emitter:Finish()
end
