AddCSLuaFile()
--[[
	Chamber RP Property Controller
	This is the device to buy properties.
	---
	Argochamber interactive 2016
]]--


AddCSLuaFile();
DEFINE_BASECLASS( "base_anim" );

--[[--------------------------------------------------------------------
								SHARED
]]----------------------------------------------------------------------

ENT.PrintName		= "Property Controller"
ENT.Category		= "Chamber RP - Property"

ENT.Author			= "DyaMetR"
ENT.Contact			= "admin@argochamber.com"
ENT.Purpose			= "To buy a property."
ENT.Instructions	= "Use to buy property."
ENT.Model			= "models/hunter/plates/plate1x1.mdl"
ENT.Spawnable		= false


--[[--------------------------------------------------------------------
								CLIENTSIDE
]]----------------------------------------------------------------------
if CLIENT then

	surface.CreateFont( "crp_propcontrol1", {
    font = "Roboto",
    size = 34,
    weight = 1000,
    blursize = 0,
    scanlines = 0,
    antialias = true,
    underline = false,
    italic = false,
    strikeout = false,
    symbol = false,
    rotary = false,
    shadow = false,
    additive = false,
    outline = false
    })
	
	surface.CreateFont( "crp_propcontrol2", {
    font = "Roboto",
    size = 24,
    weight = 500,
    blursize = 0,
    scanlines = 0,
    antialias = true,
    underline = false,
    italic = false,
    strikeout = false,
    symbol = false,
    rotary = false,
    shadow = false,
    additive = false,
    outline = false
    })
	
	surface.CreateFont( "crp_propcontrol3", {
    font = "Roboto",
    size = 24,
    weight = 1000,
    blursize = 0,
    scanlines = 0,
    antialias = true,
    underline = false,
    italic = false,
    strikeout = false,
    symbol = false,
    rotary = false,
    shadow = false,
    additive = false,
    outline = false
    })

	function ENT:Draw()
	
		if self:GetColor().a > 0 then
		
			self:DrawModel()
			
		end
	
		cam.Start3D2D( self:GetPos() + self:GetAngles():Up()*2, self:GetAngles(), 0.25 );
			
			draw.RoundedBox(0,-94,-94,188,188,Color(255,255,255,100*(self:GetColor().a/255)));
			draw.RoundedBox(0,-92,-92,184,46,Color(245,0,0,150*(self:GetColor().a/255)));
			draw.SimpleText("ON SALE!", "crp_propcontrol1", 0, -70, Color(255,255,255,255*(self:GetColor().a/255)),1,1);
			
			surface.SetDrawColor(Color(255,255,255, 255*(self:GetColor().a/255)));
			surface.SetMaterial(Material("icon16/building.png"))
			surface.DrawTexturedRect(-84, -30, 16, 16)
			draw.SimpleText(self:GetNWString("name"), "crp_propcontrol2", -64, -33, Color(255,255,255,255*(self:GetColor().a/255)));
			
			surface.SetMaterial(Material("icon16/money.png"))
			surface.DrawTexturedRect(-84, 10, 16, 16)
			draw.SimpleText("$ "..self:GetNWInt("cost"), "crp_propcontrol2", -64, 5, Color(255,255,255,255*(self:GetColor().a/255)));
			
			draw.RoundedBox(0,-88,46,178,26,Color(200,255,200,100*(self:GetColor().a/255)));
			draw.SimpleText("Press 'USE' to buy!", "crp_propcontrol3", 0, 60, Color(255,255,255,255*(self:GetColor().a/255)),1,1);
			
		cam.End3D2D();
	
	end

	function ENT:Initialize()

	end

end

--[[--------------------------------------------------------------------
								SERVERSIDE
]]----------------------------------------------------------------------
if SERVER then

	function ENT:KeyValue(key,value)
	
		if key == "uid" then
		
			self:SetNWString("UID", value);
			
		elseif key == "p" then
		
			self:SetAngles(Angle(self:GetAngles().p + value,self:GetAngles().y,self:GetAngles().r))
			
		elseif key == "y" then
		
			self:SetAngles(Angle(self:GetAngles().p,self:GetAngles().y + value,self:GetAngles().r))
			
		elseif key == "r" then
		
			self:SetAngles(Angle(self:GetAngles().p,self:GetAngles().y,self:GetAngles().r + value))
		
		end
		
	end

	function ENT:Initialize()
	
		-- Physics initialize.
		self:SetModel( self.Model );
		self:PhysicsInit( SOLID_NONE );
		self:SetMoveType( MOVETYPE_NONE );
		self:SetSolid( SOLID_VPHYSICS );
		self:SetColor(Color(0,0,0,255));

	end
	
	function ENT:Think()
	
		if self:GetNWString("name") == "" then
		
			for k,v in pairs(ents.GetAll()) do
			
				if v:GetClass() == "property" then
				
					if v.Class != nil and v.Class.UID == self:GetNWString("UID") then

						self:SetNWInt("cost", v.Class.cost);
						self:SetNWString("name", v.Class.defaultName);
					
					end
				
				end
			
			end
		
		end
	
	end
	
	function ENT:Use(ply, caller, SIMPLE_USE)
	
		for k,v in pairs(ents.GetAll()) do
				
			if v:GetClass() == "property" and v.Class.UID == self:GetNWString("UID") and v.Class.owner == nil then
			
				if (ply:GetNWInt("money") - v.Class.cost) >= 0 then
				
					v.Class.own(ply)
					self:SetColor(Color(0,0,0,0))
					ply:SetNWInt("money", ply:GetNWInt("money") - v.Class.cost)
					
				else
				
					SendHint(ply, "You can't afford buying "..(v.Class.defaultName)..".", 2, Color(245,0,0))
				
				end
			
			end
		
		end
	
	end
	
end