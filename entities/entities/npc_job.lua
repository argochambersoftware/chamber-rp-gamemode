AddCSLuaFile()

ENT.Base = "base_ai" -- This entity is based on "base_ai"
ENT.Type = "ai" -- What type of entity is it, in this case, it's an AI.
ENT.AutomaticFrameAdvance = true -- This entity will animate itself.
ENT.PrintName = "Employment NPC"
ENT.Spawnable = true
ENT.Category = "Chamber Roleplay"
 
function ENT:SetAutomaticFrameAdvance( bUsingAnim )
	self.AutomaticFrameAdvance = bUsingAnim
end

if SERVER then

	util.AddNetworkString("crp_npc_job")

	function ENT:Initialize( )
		
		self:SetModel( "models/humans/group01/male_07.mdl" )
		self:SetHullType( HULL_HUMAN )
		self:SetHullSizeNormal( )
		self:SetNPCState( NPC_STATE_SCRIPT )
		self:SetSolid(  SOLID_BBOX )
		self:CapabilitiesAdd( CAP_ANIMATEDFACE || CAP_TURN_HEAD )
		self:SetUseType( SIMPLE_USE )
		self:DropToFloor()
		
		self:SetMaxYawSpeed( 90 )
		
	end

	function ENT:OnTakeDamage()
		return false
	end 

	function ENT:AcceptInput( Name, Activator, Caller )	

		if Name == "Use" and Caller:IsPlayer() then
			
			net.Start("crp_npc_job")
			net.WriteString(Caller.Job)
			net.Send(Caller)
			
		end
		
	end

end

if CLIENT then
	
	surface.CreateFont( "crp_npc_job1", {
    font = "Roboto",
    size = 20,
    weight = 1000,
    blursize = 0,
    scanlines = 0,
    antialias = true,
    underline = false,
    italic = false,
    strikeout = false,
    symbol = false,
    rotary = false,
    shadow = false,
    additive = false,
    outline = false
    })
	
	local function OpenMenu(yourjob)
		
		local colr = GetConVar("crp_hud_frame_r"):GetInt()
		local colg = GetConVar("crp_hud_frame_g"):GetInt()
		local colb = GetConVar("crp_hud_frame_b"):GetInt()
		
		local Frame = vgui.Create("DFrame");
		Frame:SetSize(400,400);
		Frame:SetPos((ScrW()/2) - Frame:GetWide()/2, (ScrH()/2) -  Frame:GetTall()/2);
		Frame:SetTitle("");
		Frame:ShowCloseButton(false);
		Frame:MakePopup();
		Frame.Title = "Employment Office";
		
		local CloseButton = vgui.Create("DButton", Frame);
		CloseButton:SetSize(30,16);
		CloseButton:SetPos(Frame:GetWide() - 38, 7);
		CloseButton:SetText("");
		CloseButton.Paint = function()
		
			draw.RoundedBox(0,0,0,CloseButton:GetWide(), CloseButton:GetTall(), Color(230,0,0,150));
			draw.SimpleText("X", "default", CloseButton:GetWide()/2,2,Color(255,255,255,100),1)
		
		end;
		CloseButton.DoClick = function()
		
			Frame:Close();
		
		end;
		
		--[[
		
			JOB LIST
		
		]]--
		
		local sheet = vgui.Create( "DPropertySheet", Frame )
		sheet:SetPos(2,30)
		sheet:SetSize(Frame:GetWide() - 4, Frame:GetTall() - 74)
		
		local fire = vgui.Create("DButton", Frame)
		fire:SetPos(2,358)
		fire:SetSize(Frame:GetWide() - 4, 40)
		
		if yourjob == "CITIZEN" then
		
			fire:SetText("You're unemployed")
			fire:SetEnabled(false)
			
		else
		
			fire:SetText("Quit "..getJobs()[yourjob].name)
			fire:SetEnabled(true)
			
		end
		fire.DoClick = function()
		
			net.Start("crp_npc_job")
			net.WriteString("CITIZEN")
			net.SendToServer()
			
			Frame:Close()
			
		end

		local panel1 = vgui.Create( "DPanelList", sheet )
		sheet:AddSheet( "Goverment", panel1, "icon16/shield.png", false, false, "Goverment related jobs. Salary is set by the president." )

		local panel2 = vgui.Create( "DPanelList", sheet )
		sheet:AddSheet( "Comercial", panel2, "icon16/money.png", false, false, "Jobs related to comercial activities. Can setup jobs and sell items legally." )
		
		local panel3 = vgui.Create( "DPanelList", sheet )
		sheet:AddSheet( "Contract", panel3, "icon16/user_suit.png", false, false, "Contract jobs. Salary is set by your recruiter." ) 
		
		for k,v in pairs(getJobs()) do
		
			if v.category != "none" then
			
				local Job = vgui.Create("DPanel", Frame)
				Job:SetPos(0,0)
				Job:SetSize(375,68)
				
				local Paint = vgui.Create("DPanel", Job)
				Paint:SetPos(0,0)
				Paint:SetSize(Job:GetWide(), Job:GetTall())
				Paint.Paint = function()
				
					draw.RoundedBox(0, 2,2, 64,64,Color(255,255,255,255))
					
					if v.category == "goverment" then
					
						draw.SimpleText(v.name, "crp_HUD2", 70,15,v.colour)
						draw.SimpleText("Salary: 99$", "crp_npc_job1", 70,34,Color(0,0,0,255))
						
					else
					
						draw.SimpleText(v.name, "crp_HUD2", 70,20,v.colour)
					
					end
				
				end
				
				local outcome = ""
				
				if math.random(1,2) == 1 then
				
					outcome = "male_0"..(math.random(1,9))
				
				else
				
					outcome = "female_0"..(math.random(1,7))
				
				end
				
				local SpawnI = vgui.Create( "SpawnIcon" , Job ) -- SpawnIcon
				SpawnI:SetPos( 2, 2 )
				
				if v.model == "citizen" then
				
					if outcome == "female_05" then
					
						SpawnI:SetModel( "models/player/Group01/female_01.mdl" )
					
					else
					
						SpawnI:SetModel( "models/player/Group01/"..outcome..".mdl" )
						
					end
					
				elseif v.model == "rebel" then
				
					if outcome == "female05" then
					
						SpawnI:SetModel( "models/player/Group03/female01.mdl" )
					
					else
					
						SpawnI:SetModel( "models/player/Group03/"..outcome..".mdl" )
						
					end
				
				elseif v.model == "metrocop" then
				
					SpawnI:SetModel( "models/player/Police.mdl" )
				
				elseif v.model == "president" then
				
					SpawnI:SetModel( "models/player/Breen.mdl" )
				
				end
				
				SpawnI:SetDisabled(true)
				
				local button = vgui.Create("DButton", Job)
				button:SetSize(100,20)
				button:SetPos(Job:GetWide() - 110,Job:GetTall() - 30)
				button:SetText("Become")
				button:SetToolTip(v.description)
				button.DoClick = function()
				
					net.Start("crp_npc_job")
					net.WriteString(k)
					net.SendToServer()
					
					Frame:Close()
				
				end
				
				if yourjob == k then
				
					button:SetEnabled(false)
				
				end
			
				if v.category == "goverment" then
				
					panel1:AddItem(Job)
				
				elseif v.category == "comercial" then
				
					panel2:AddItem(Job)
				
				elseif v.category == "contract" then
				
					panel3:AddItem(Job)
				
				end
				
			end
		
		end
		
	end
	
	net.Receive("crp_npc_job", function(len)
	
		OpenMenu(net.ReadString())
	
	end);

end