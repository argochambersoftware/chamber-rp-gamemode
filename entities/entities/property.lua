AddCSLuaFile()
--/**
-- * Argochamber Roleplay Property Brush
-- */

ENT.Type 		= "brush"

ENT.Category	= "Argochamber Roleplay - Map entities"

if SERVER then

ENT.Class = nil

function ENT:KeyValue(key, value)

	if not self.Sync then self.Sync = {}; end
	
	self.Sync[key] = value;
	
end;

function ENT:Think()

	if (self.Sync != nil) and (self.Sync["uid"] != nil and self.Sync["defaultName"] != nil and self.Sync["cost"] != nil and self.Sync["ownable"]) then
	
		self.Class = Property(self.Sync["uid"], self.Sync["defaultName"], self.Sync["cost"] , self.Sync["ownable"]);
		self.Sync = nil;
	
	end;

end;

function ENT:StartTouch(ent)

	if ent:IsPlayer() and ent:Alive() then
	
		ent.InProperty = self.Class.UID
	
		if self.Class.owner != nil then
		
			if ent == self.Class.owner then
			
				timer.Simple(1, function() SendHint(ent, "Press 'F4' to enter the Property Menu!", 2, Color(0,180,255)) end)
				SendHint(ent,"Welcome to "..(self.Class.customName)..".",0)
				
			else
			
				SendHint(ent,"Welcome to "..self.Class.customName..". Owned by "..self.Class.owner:Nick()..".",0)
			
			end
			
		else
			
			SendHint(ent,"Welcome to "..self.Class.customName..". On sale for $"..self.Class.cost..".",1,Color(0,130,200))
			
		end
	
	end

end;

function ENT:EndTouch(ent)

	if ent:IsPlayer() then
	
		if self.Class.owner == ent then
		
			net.Start("crp_property_forceclose")
			net.Send(ent)
			
		end
		
		ent.InProperty = nil
	
	end

end

end