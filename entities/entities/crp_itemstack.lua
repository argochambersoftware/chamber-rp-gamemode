AddCSLuaFile()
--[[
	Chamber RP Stack of items
	This is the physical representation of an stack.
	---
	Argochamber interactive 2016
]]--


AddCSLuaFile();
DEFINE_BASECLASS( "base_anim" );

--[[--------------------------------------------------------------------
								SHARED
]]----------------------------------------------------------------------

ENT.PrintName		= "Item Stack"
ENT.Category		= "Chamber RP - Items"

ENT.Author			= "sigmasoldier"
ENT.Contact			= "admin@argochamber.com"
ENT.Purpose			= "To store stacks out of the containers and inventories."
ENT.Instructions	= "This will have a single stack that can be dropped."
ENT.Model			= "models/combine_helicopter/helicopter_bomb01.mdl"
ENT.Spawnable		= true


function ENT:SetupDataTables()

	self:NetworkVar( "Int", 0, "StackSize" );
	self:NetworkVar( "Int", 1, "ItemUID" );

end

--[[--------------------------------------------------------------------
								CLIENTSIDE
]]----------------------------------------------------------------------
if CLIENT then

	local SIZE = 50;
	local TURN_SPEED = 0.6;
	local FLOAT_SPEED = 0.01;
	local FLOAT_FACTOR = 3;
	local FLOAT_OFFSET = 10;
	local GLOW_RADIUS = 64;

	local PLATE_HEIGHT = 40;
	local PLATE_WIDTH = 64;

	local GLOW_MATERIAL = Material("sprites/light_glow02_add");

	--[[
		This will draw the stack of items down.
		@override
	]]
	function ENT:Draw()
		local pos = self:GetPos() + Vector(0, 0, math.sin(self.float)*FLOAT_FACTOR + FLOAT_OFFSET);
		local ang = Angle(0, self.angle, 0);

		cam.Start3D(); -- Start the 3D function so we can draw onto the screen.
			render.SetMaterial( GLOW_MATERIAL );
			render.DrawSprite( pos, GLOW_RADIUS, GLOW_RADIUS, Color(255,255,255) );
		cam.End3D();

		cam.Start3D2D(pos + Vector(0,0,30), ang + Angle(0,0,90), 0.2);
			draw.RoundedBox(4, -PLATE_WIDTH/2, -PLATE_HEIGHT/2, PLATE_WIDTH, PLATE_HEIGHT, Color(0,0,0,200));
			draw.SimpleText( "x"..self.Ssize, "DermaLarge", 0, -PLATE_HEIGHT/2.6, Color(255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP );
		cam.End3D2D();

		self.csProp:SetPos(pos);
		self.csProp:SetAngles(ang);
	end

	function ENT:Think()
		self.angle = self.angle + TURN_SPEED;
		self.float = self.float + FLOAT_SPEED;
		if self.angle >= 360 then self.angle = 0; end
		if self.float >= 360 then self.float = 0; end
	end

	function ENT:Initialize()
		self.Ssize = self:GetStackSize();
		self.Sitem = Chamber.getItem(self:GetItemUID());

		self.csProp = ClientsideModel(self.Sitem:getDrawModel());

		self.csProp:SetModelScale( (1.0/self.csProp:GetModelRadius())*10.0, 0);

		self.angle = 0;
		self.float = 0;
	end

end

--[[--------------------------------------------------------------------
								SERVERSIDE
]]----------------------------------------------------------------------
if SERVER then

	function ENT:Initialize()
		-- Physics initialize.
		self:SetModel( self.Model );
		self:PhysicsInitSphere( 5, "roller" );
		self:SetMoveType( MOVETYPE_VPHYSICS );
		self:SetSolid( SOLID_VPHYSICS );
		self:SetCollisionGroup( COLLISION_GROUP_DEBRIS );

		local phys = self:GetPhysicsObject();
		if ( phys:IsValid() ) then
			phys:Wake();
		end

		self:SetFriction(50000);
	end

	-- This is a "Post spawn script", initializes the entity.
	function ENT:Build(stackdata)
		self:SetStackSize(stackdata:getAmount());
		self:SetItemUID(stackdata:getItem():getId());
	end
	
	function ENT:Think()
		
	end

	function ENT:Use()
		print(self:GetStackSize());
	end
	
end