--[[
	Item object - Storeable items
	Chamber RP
	---
	Argochamber Interactive 2016
]]

local DEFAULT_MODEL = "models/props_junk/watermelon01.mdl";
local DEFAULT_MAX_STACK = 64;


function Item(name, preview, ient, imeta, iMaxStackSize)
	local o = {};

	if not name then Chamber.log("An item must have a name!", Chamber.log.Level.Error, "classes/item.class"); return; end
	-- Else if the name was correct, invoke the item manager.

	local entity = ient or NULL; -- This is the physical object that represents the item
	local itemName = name; -- The display name
	local maxStackSize = iMaxStackSize or DEFAULT_MAX_STACK; -- How much we can stack up?
	local model = preview or DEFAULT_MODEL; -- The preview model.
	local meta = imeta or {}; -- Metadata atributes, like model = "/.../prop.mdl" for props and so on.

	o.getName = function(self)
		return itemName;
	end

	o.getMaxStackSize = function(self)
		return maxStackSize;
	end

	o.getDrawModel = function(self)
		return model;
	end

	-- To avoid errors when leaking an object throught the constructor, register at the very end.
	local id = Chamber.registerItem(o);

	o.getId = function()
		return id;
	end

	--[[-----------------------------------------
			# Post Construction methods #
			All will return the object.
	--]]-----------------------------------------

	function o:SetModelScale(scl)
		meta["modelscale"] = scl;
		return self;
	end

	return o;
end