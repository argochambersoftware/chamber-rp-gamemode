--[[
	Container Object - This is the serverside container that holds down information about the stacks stored within.
	Chamber RP
	---
	Argochamber Interactive 2016
]]

Chamber.containers = {};
Chamber.containers.NET_CONTAINER_REQUEST = "REQUEST CONTAINER CONTENTS";
Chamber.containers.NET_INVENTORY_REQUEST = "REQUEST PLAYER INVENTORY CONTENTS";

hook.Add("CModulesLoaded", "Chamber Container Load modules", function()
	-- This is called once all modules are loaded.
	if SERVER then
		Chamber.Net.pool(Chamber.containers.NET_CONTAINER_REQUEST);
		Chamber.Net.pool(Chamber.containers.NET_INVENTORY_REQUEST);
	end
end);

if SERVER then

	--[[
		Universal item container.
		Holds down stacks of items in a given entity.
		Beware! the entity must be real and physical!
		@param iUnique Mixed This will identify the container throught the network. USE SOMETHING UNIQUE PLEASE!
		@param size number Container size.
	]]
	function Container(iUnique, size)

		if not iUnique then Chamber.log("Every container must have a Unique Key!", Chamber.log.Level.Error, "classes/container.class"); return; end

		local o = {};

		local ent = iUnique;
		local stacks = {};
		local capacity = size or 9;

		--[[
			Adds an stack to the container, returns back the Stack UID (SUID)
			@param stk Stack The input stack.
			@return int Stack UID
		]]--
		function o:addStack(stk)
			table.insert(stacks, stk);
			return #stacks;
		end

		--[[
			Returns the stack data of the given Stack Unique Identitfier.
			@param SUID number the stack UID.
			@return Stack the stack data.
		]]
		function o:getItem(SUID)
			return stacks[SUID];
		end

		--[[
			Internally, it will split the stack given in two with a proportion given in first.
			If first exceeds or equals the amount of this stack, the operation is aborted, because that would mean that we're splitting this stack into a single new one.
			The second one is the amount that lefts from the first one.
			@param SUID number Stack UID
			@param first number Stack A Amount.
			@return Stack new stack created reference
		]]
		function o:splitStack(SUID, first)
			-- Ensure that we're not splitting into a negative amount stack.
			if first >= stacks[SUID]:getAmount() then return SUID; end
			--Now if the amount is OK, do the thing...
			local second = math.abs( stacks[SUID]:getAmount() - first );
			local item = stacks[SUID]:getItem();
			stacks[SUID] = Stack( item, first );
			local stk = Stack( item, second );
			self:addStack(stk);
			return stk;
		end

		--[[
			Encodes the data to a LUON array to be sent into the net.
			Seralized data can be interpreted by the DInventoryPanel or DContainer.
			@return Table Serialized data
		]]
		function o:serialize()
			local s = {};
			for k, v in pairs(stacks) do
				s[tostring(k)] = {["id"] = v:getItem():getId(), ["qty"] = v:getAmount()};
				--print(k.." -> Serialize encode "..v:getItem():getId().." : "..v:getAmount());
			end
			return s;
		end

		--[[
			Drops an item to the world.
			@param number Stack UID to be droped
			@param opPos Vector Optional Position when spawning not for a player (IE: NULL or a Chest).
		]]
		function o:dropStack(SUID, opPos)
			 --Before spawning, ensure that we can spawn it.
			local spawnpos = Vector(0,0,0);
			if ent ~= NULL then
				if ent:IsPlayer() == true then
					local trace = {};
			        trace.start = ent:EyePos();
			        trace.endpos = trace.start + ent:GetAimVector() * 85;
			        trace.filter = ent;

			        local tr = util.TraceLine(trace);
			        spawnpos = tr.HitPos;
				else
					spawnpos = ent:GetPos() + Vector( 0, 0, 100 );
					spawnpos = opPos or spawnpos;
				end
			else
				if not opPos then
					Chamber.log("Failed to drop the stack! The position was not given and the container is not inside an entity!", Chamber.log.Level.Error, "classes/container.class");
					return false;
				else
					spawnpos = opPos;
				end
			end

			-- Create the entity
			local e = ents.Create("crp_itemstack");
			if ( !IsValid( e ) ) then return; end
			
			-- Build, spawn, position.
			e:Build(stacks[SUID]);
			e:SetPos( spawnpos );
			e:Spawn();
			table.remove(stacks, SUID);
		end

		return o;
	end

	--[[
		This function will send a net message to a player telling what we have in those containers.
		@param ent Entity
		@param ply Player
	]]
	local function getContainerFor(ent, ply)
		net.Start(Chamber.containers.NET_CONTAINER_REQUEST);
			net.WriteTable(ent.inventory:serialize());
		net.Send(ply);
	end

	net.Receive(Chamber.containers.NET_CONTAINER_REQUEST, function(len, ply) 
		local ent = net.ReadEntity();
		getContainerFor(ent, ply);
	end);
	net.Receive(Chamber.containers.NET_INVENTORY_REQUEST, function(len, ply) 
		getContainerFor(ply, ply);
	end);

end

if CLIENT then

	--[[
		Container getter.
		Gets the contents of the container from the net.
		@param [housing] the physical item that represents the container. If nil, LocalPlayer is assumed.
		@global
		@chamber
	]]--
	Chamber.getContainer = function(housing)
		if housing then
			net.Start(Chamber.containers.NET_CONTAINER_REQUEST);
				net.WriteEntity(housing);
			net.SendToServer();
		else
			net.Start(Chamber.containers.NET_INVENTORY_REQUEST);
				--Nothing more to write! since the player already comes with the message!
			net.SendToServer();
		end
	end


	--[[
		Single container slot parent class VGUI
		@panel
	]]--
	do
		local DEFAULT_SCALE = 64;
		local DEFAULT_MARGIN = 5;

		local PANEL = {};

		PANEL.margin = DEFAULT_MARGIN;
		PANEL.scale = DEFAULT_SCALE;
		PANEL.dragId = "ItemStack";

		function PANEL:Init()
			-- Set the parent panel as the main entity
			self:SetSize( self.scale+self.margin*2, self.scale+self.margin*2 );
		end

		-- Sets the size of the slot.
		function PANEL:setScale(scl)
			self.scale = scl;
			self:update();
		end

		-- Sets the inner margins of the slot.
		function PANEL:setMargin(mar)
			self.margin = mar;
			self:update();
		end

		-- Gets the inner margins of the slot.
		function PANEL:getMargin()
			return self.margin;
		end

		-- Gets the size of the slot.
		function PANEL:getScale()
			return self.scale;
		end

		-- Recive function, can be overriden in child classes.
		function PANEL:receive(panels, isDropped, menuIndex, mouseX, mouseY)
			if isDropped then
				
			else
			end
		end

		-- You should not override this, is a getter to identify the Drag Name. If changed may the drag'n'drop operations fail between parent classes and child classes.
		function PANEL:getDragId()
			return self.dragId;
		end

		vgui.Register( "DContainerSlot", PANEL, "CPanel" );
	end

	--[[
		Single container slot parent class VGUI
		@panel
	]]--
	do
		local DEFAULT_WIDTH = 168;
		local DEFAULT_HEIGHT = 64;

		local PANEL = {};

		PANEL.width = DEFAULT_WIDTH;
		PANEL.height = DEFAULT_HEIGHT;

		function PANEL:Init()
			-- Set the parent panel as the main entity
			self:SetSize( self.width, self.height);
			self:Center();
			self:MakePopup();
			self:SetBackgroundBlur(true);
			self:SetTitle("Drop amount...");

			self.button = vgui.Create("DButton", self);
			self.button:SetPos(self.width/2 + 2, self.height/2);
			self.button:SetText("Drop");

			self.spin = vgui.Create("DNumberWang", self);
			self.spin:SetPos(8, self.height/2 + 1);
			self.spin:SetMin(1);
			self.spin:SetMax(63);
		end

		function PANEL:GetValue()
			return self.spin:GetValue();
		end

		vgui.Register( "DI_DropAmount", PANEL, "CFrame" );
	end

	--[[
		A stack of items for the container
		@panel
	]]--
	do
		local PANEL = {};

		local DEFAULT_SPAWNICON = "models/maxofs2d/cube_tool.mdl";

		function PANEL:Init()
			self.item = NULL;
			self.count = 1;
			self.iiuid = -1;

			self.model = vgui.Create("SpawnIcon", self);
			self.model:SetPos( self:getMargin(), self:getMargin());
			self.model:SetSize( self:getScale(), self:getScale() );
			self.model:SetModel(DEFAULT_SPAWNICON);
			
			self.dev = vgui.Create("DLabel", self);
			self.dev:SetColor(Color(0,0,0));
			self.dev:SetPos(2,0);

			self.model.DoClick = function()
				self.actions = vgui.Create("DMenu", self:GetParent():GetParent());
				local dropAct = self.actions:AddOption("Drop", function()
					-- This tells the server to drop a given stack.
					net.Start(DROP_MSG);
						net.WriteInt(self.iiuid, 32);
					net.SendToServer();
					self:getInventory():GetParent():Close(); --:removeItem(self); -- Old method expects that all IDs to be the same, new one refreshes the GUI.
					Chamber.getContainer();
				end);
				dropAct:SetIcon("icon16/arrow_down.png");
				local dropHalfAct = self.actions:AddOption("Drop Quantity", function()
					local dropMem = vgui.Create("DI_DropAmount");
					local uid = self.iiuid;
					dropMem.button.DoClick = function(dermis)
						-- This tells the server to drop a given stack.
						net.Start(DROP_MSG_AMT);
							net.WriteInt(uid, 32);
							net.WriteInt(dropMem.spin:GetValue(), 32);
						net.SendToServer();
						dropMem:Close();
					end
				end);
				dropHalfAct:SetIcon("icon16/package_go.png");
				self.actions:Open();
			end

			self.countLabel = vgui.Create("CLabel", self);
			self.countLabel:SetText(self.count);
			self.countLabel:SetFont("DermaLarge");
			self.countLabel:Dock(BOTTOM);
			self.countLabel:SetTextColor(Color(0,0,0));

		end

		function PANEL:build(iItem, iCount, SUID)
			self.item = iItem;
			self.count = iCount;

			self:updateLabel();

			self.iiuid = SUID or -1;
			self.model:SetModel(self.item:getDrawModel());

			self.dev:SetText("##"..self.iiuid);

			self.model:RebuildSpawnIcon();
		end

		function PANEL:getInventory()
			return self:GetParent():GetParent():GetParent():GetParent():GetParent();
		end

		-- Updates the label of the count.
		function PANEL:updateLabel()
			self.countLabel:SetText(self.count);
		end

		-- Updates the whole panel.
		function PANEL:update()
			self:SetPos( self:getMargin(), self:getMargin() );
			self:SetSize( self:getScale()+self:getMargin(), self:getScale()+self:getMargin() );
			self.model:SetPos(self:getMargin(), self:getMargin());
			self.model:SetSize( self:getScale(), self:getScale() );
			--self.model:RebuildSpawnIcon();
			self:updateLabel();
		end

		-- Sets the amount of the stack.
		function PANEL:setAmount(amt)
			self.count = amt or self.count;
			self:updateLabel();
		end

		vgui.Register( "DItemStack", PANEL, "DContainerSlot" );
	end	

	--[[
		Container VGUI
		@panel
		Isolate blocks with do ... end
	]]--
	do
		local PANEL = {};

		local DEFAULT_MARGIN = 5;
		local DEFAULT_SIZE = 16;

		function PANEL:Init()
			self:SetBackgroundColor(Color(100,100,100,100));
			
			-- This is used to scroll items.
			self.Scroll = vgui.Create( "DScrollPanel", self );
			self.Scroll:Dock(FILL);
			self.Scroll:SetPos( 10, 30 );

			-- This is the list of stacks in itself.
			self.List = vgui.Create( "DIconLayout", self.Scroll );
			self.List:Dock(FILL);
			self.List:SetPos( 0, 0 );
			self.List:SetSpaceY( DEFAULT_MARGIN );
			self.List:SetSpaceX( DEFAULT_MARGIN );

			self.used = 0;
			self.size = DEFAULT_SIZE;
		end

		function PANEL:build(size)
			self.size = size;
		end

		function PANEL:setMargins(mar)
			self.List:SetSpaceY( mar );
			self.List:SetSpaceX( mar );
		end

		function PANEL:addItem(itemPanel)
			self.List:Add(itemPanel);
		end
		
		-- Gets the free space left.
		function PANEL:getFreeSpace()
			return self.size - self.used;
		end

		-- Gets the total capacity.
		function PANEL:getCapacity()
			return self.size;
		end

		function PANEL:setCapacity(s)
			self.size = s;
		end

		-- Gets the Used space.
		function PANEL:getUsedSpace()
			return self.used;
		end

		-- Add a single stack.
		function PANEL:addStack(stack, SUID)
			self.used = self.used+1;
			self:addItem( stack:getItemStack(nil, SUID) );
		end

		--Finish construction by adding elements:
		function PANEL:addStacks(stacks)
			for k, v in pairs(stacks) do
				self.used = self.used+1;
				self:addItem( v:getItemStack() );
			end
		end

		--[[
			Load serial data from the network.
			This will load the data that has been serialized from a container and setn throught the network.
			@param Table Stacks Serialized
		]]
		function PANEL:loadSerialData(stackdata)
			table.sort(stackdata);
			for k, v in pairs(stackdata) do
				self:addStack( Stack( Chamber.getItem(v["id"]), v["qty"] ), k );
			end
		end

		--[[
			Removes a stack from the container. The item goes nowhere.
			@param DItemStack Derma panel that is being removed.
		]]
		function PANEL:removeItem(item)
			self.used = self.used-1;
			item:Remove();
		end

		vgui.Register( "DContainer", PANEL, "CPanel" );
	end
end