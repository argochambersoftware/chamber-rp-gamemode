--[[
	Property Object
	Chamber RP
	---
	Argochamber Interactive 2016
]]

function Property(uid, name, cost, ownable)
	local o = {};
	
	o.UID = uid;
	o.defaultName = name;
	o.customName = name;
	o.cost = cost;
	o.ownable = ownable;
	o.owner = nil;
	o.coowners = {};
	o.isShop = false;
	
	o.own = function(ply)
		
		if o.owner == nil then
		
			o.owner = ply;
			SendHint(ply, "You've bought "..(o.customName).." for $"..(o.cost)..".",1,Color(30,200,0,100));
		
		end;
	
	end;
	
	o.disown = function(ply)
	
		if o.owner == ply then
		
			o.owner = nil;
			o.customName = o.defaultName;
			SendHint(ply, "You've sold "..(o.customName).." for $"..math.floor(o.cost/2)..".",1,Color(200,30,0,100));
		
		end;
	
	end;
	
	o.addcoowner = function(coowner)
	
		if !table.HasValue(o.coowners, coowner) then
		
			table.insert(o.coowners,coowner);
		
		end
	
	end;
	
	o.removecoowner = function(k)
	
		if table.HasValue(o.coowners, k) then
			
			table.remove(o.coowners, k);
		
		end
	
	end;
	
	o.toggleShop = function()
	
		if o.isShop then
		
			o.isShop = false;
			
		elseif !o.isShop then
		
			o.isShop = true;
		
		end
	
	end;
	
	return o;
end