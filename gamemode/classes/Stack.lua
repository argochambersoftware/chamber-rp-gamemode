--[[
	Item Stack - Stack of items
	Chamber RP
	---
	Argochamber Interactive 2016
]]


local DEFAULT_MAX_STACK_SIZE = 64;

if not Chamber.util then Chamber.util = {}; end
Chamber.util.Stack = {
	serialToStack = function(tbl)
		return Stack(Chamber.getItem(tbl["id"]), tbl["amount"]);
	end
};

function Stack(iItem, iAmount)
	local o = {};

	local amount = iAmount or 1;
	local item = iItem;
	local stackSize = item:getMaxStackSize() or DEFAULT_MAX_STACK_SIZE;

	function o:getAmount()
		return amount;
	end

	function o:getStackSize()
		return stackSize;
	end

	function o:getItem()
		return item;
	end

	-- Returns the ItemStack equivalent.
	if CLIENT then
		function o:getItemStack(dermaParent, SUID)
			local pnl = vgui.Create("DItemStack", dermaParent);
			pnl:build(item, amount, SUID);
			return pnl;
		end
	end

	if SERVER then
		function o:assembleEntity()
			local ent = ents.Create("crp_itemstack");
			ent:Build(self);
			return ent;
		end
	end

	--[[
		Serializes a single item
		@return Table serial data
	]]
	function o:serialize()
		return {["id"] = item:getId(), ["amount"] = amount};
	end
	
	return o;
end