--[[
	Job Object
	Chamber RP
	---
	Argochamber Interactive 2016
]]

function Job(name, col, category, model, weapon, desc)
	local o = {};
	
	o.name = name or "NULL";
	o.colour = col or Color(255,255,255);
	o.category = category; -- "none", "goverment", "comercial", "freelancer"
	o.model = model or "citizen" -- "citizen", "rebel", "metrocop", "soldier", "supersoldier", "president"
	o.loadout = weapon or {};
	o.description = desc or [[No description available.]];
	
	return o;
end
