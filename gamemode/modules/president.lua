--[[
	President module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Presidential affairs";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "The purpose of this module is to manage the president job and the town clockwork.";

function Module:think()

	addJob("PRESIDENT", "President", Color(240,0,0), "none", "president")
	
end