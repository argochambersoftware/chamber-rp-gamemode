--[[
	Contracts module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Contracting";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "The purpose of this module is to manage all the contracts and the jobs that can do this.";

function Module:think()

	addJob("GUARD", "Security Guard", Color(0,180,255), "contract", "rebel")

end