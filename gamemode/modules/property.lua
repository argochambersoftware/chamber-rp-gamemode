--[[
	Properties module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Properties";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "Properties module, to own and disown properties.";

if SERVER then

	util.AddNetworkString("crp_property_menu")
	util.AddNetworkString("crp_property_forceclose")

	local function isDoor(ent)
	
		if ent:GetClass() == "prop_door_rotating" or ent:GetClass() == "prop_door" or ent:GetClass() == "func_door" or ent:GetClass() == "func_door_rotating" then
		
			return true
			
		else
		
			return false
		
		end
	
	end

	function Module.initpostent()
	
		for k,v in pairs(ents.GetAll()) do
		
			if isDoor(v) and v:GetName() != "" and string.find(v:GetName(), "$") != nil then
				
				start, endpos, str = string.find(v:GetName(), "$",1, true)
				
				v.UID = string.sub(v:GetName(),1,endpos - 1)
			
			end
		
		end
	
	end
	
	local function OpenMenu(ply)
	
		if ply.InProperty != nil then
		
			for k,v in pairs(ents.GetAll()) do
			
				if v:GetClass() == "property" then
				
					if v.Class.UID == ply.InProperty and v.Class.owner == ply then
					
						net.Start("crp_property_menu")
						net.WriteString(v.Class.customName)
						net.WriteFloat(v.Class.cost)
						net.WriteTable(v.Class.coowners)
						if getJobs()[ply.Job].category == "comercial" then
						
							net.WriteBool(true)
							
						else
						
							net.WriteBool(false)
						
						end
						if v.Class.isShop then
						
							net.WriteBool(true)
						
						end
						net.Send(ply)
					
					end
				
				end
			
			end
		
		end
	
	end
	hook.Add("ShowSpare2", "crp_property_menu", OpenMenu)
	
	net.Receive("crp_property_menu", function(len,ply)
	
		local action = net.ReadString()
		local coowner = net.ReadEntity() or nil
		
		if action == "sell" then
		
			if ply.InProperty != nil then
			
				for k,v in pairs(ents.GetAll()) do
			
					if v:GetClass() == "property_controller" then
					
						if v:GetNWString("UID") == ply.InProperty then
						
							v:SetColor(Color(0,0,0,255))
						
						end
					
					end
			
					if v:GetClass() == "property" then
					
						if v.Class.UID == ply.InProperty and v.Class.owner == ply then
						
							ply:SetNWInt("money", ply:GetNWInt("money") + math.floor(v.Class.cost/2))
							v.Class.disown(ply)
						
						end
					
					end
					
				end
				
			end
			
		elseif action == "addcoowner" then
		
			if ply.InProperty != nil then
			
				for k,v in pairs(ents.GetAll()) do
			
					if v:GetClass() == "property" and coowner != nil then
					
						if v.Class.UID == ply.InProperty and v.Class.owner == ply and !table.HasValue(v.Class.coowners, coowner) then
						
							SendHint(ply, "You've granted "..coowner:Nick().." co-owing rights.",1,Color(40,200,0,120));
							table.insert(v.Class.coowners, coowner)
						
						end
					
					end
					
				end
				
			end
			
		elseif action == "shop" then
		
			if ply.InProperty != nil then
			
				for k,v in pairs(ents.GetAll()) do
			
					if v:GetClass() == "property" then
					
						if v.Class.UID == ply.InProperty and v.Class.owner == ply then
						
							if v.Class.isShop then
							
								SendHint(ply, "You've dismantled the shop.",1,Color(200,45,10,110));
							
							else
							
								SendHint(ply, "Shop setup completed successfully.",1,Color(10,100,200,110));
							
							end
						
							v.Class:toggleShop()
						
						end
					
					end
					
				end
				
			end
			
		elseif action == "removecoowner" then
		
			if ply.InProperty != nil then
			
				for k,v in pairs(ents.GetAll()) do
			
					if v:GetClass() == "property" then
					
						if v.Class.UID == ply.InProperty and v.Class.owner == ply and table.HasValue(v.Class.coowners, coowner) then
						
							SendHint(ply, "You've revoked "..coowner:Nick().."'s co-owing rights.",1,Color(200,30,0,140));
							table.remove(v.Class.coowners, table.KeyFromValue(v.Class.coowners,coowner))
						
						end
					
					end
					
				end
				
			end
			
		else
		
			if ply.InProperty != nil then
			
				for k,v in pairs(ents.GetAll()) do
			
					if v:GetClass() == "property" then
					
						if v.Class.UID == ply.InProperty and v.Class.owner == ply then
						
							SendHint(ply,"The new name of "..(v.Class.customName).." is "..(action)..".",0)
							v.Class.customName = action
						
						end
					
					end
					
				end
				
			end
		
		end
		
	end)

end

if CLIENT then

	local property = {
	
		name = "",
		cost = 0,
		coowners = {},
		showshop = false,
		isShop = false
	
	}
	
	local forceclose = false

	local function PropertyMenu()
	
		local colr = GetConVar("crp_hud_frame_r"):GetInt()
		local colg = GetConVar("crp_hud_frame_g"):GetInt()
		local colb = GetConVar("crp_hud_frame_b"):GetInt()
	
		local Frame = vgui.Create("CFrame")
		Frame:SetSize(400,260)
		Frame:SetPos((ScrW()/2) - (Frame:GetWide()/2), (ScrH()/2) - (Frame:GetTall()/2))
		Frame:SetTitle("");
		Frame:ShowCloseButton(false);
		Frame:MakePopup();
		Frame.Title = property.name;
		
		Frame.Think = function()
		
			if forceclose then
			
				Frame:Close()
				forceclose = false
			
			end
		
		end
		
		local CloseButton = vgui.Create("DButton", Frame);
		CloseButton:SetSize(30,16);
		CloseButton:SetPos(Frame:GetWide() - 38, 7);
		CloseButton:SetText("");
		CloseButton.Paint = function()
		
			draw.RoundedBox(0,0,0,CloseButton:GetWide(), CloseButton:GetTall(), Color(230,0,0,150));
			draw.SimpleText("X", "default", CloseButton:GetWide()/2,2,Color(255,255,255,100),1)
		
		end;
		CloseButton.DoClick = function()
		
			Frame:Close();
		
		end;
		
		local panel1 = vgui.Create( "DPanel", Frame )
		panel1:SetPos(2,30)
		panel1:SetSize(Frame:GetWide() - 4, Frame:GetTall() - 32)
		panel1.Paint = function()
		
			draw.SimpleText("Co-owners", "crp_HUD2", 284,14,Color(100,100,100,255),1)
		
			draw.SimpleText("Property name", "crp_HUD2", 5,14,Color(100,100,100,255))
			draw.SimpleText("Co-owning options", "crp_HUD2", 5,64,Color(100,100,100,255))
			
			if property.showshop then
			
				draw.SimpleText("Comercial options", "crp_HUD2", 5,118,Color(100,100,100,255))
				
			end
			
			draw.RoundedBox(4,200,40,187,180,Color(100,100,100,100))

		end
		panel1:SetPos(0,10)
		panel1:Dock(FILL)
		
		local sell = vgui.Create("DButton", panel1)
		sell:SetPos(5,177)
		sell:SetSize(175,43)
		sell:SetText("Sell "..property.name.." for $"..math.floor(property.cost/2))
		sell.DoClick = function()
		
			net.Start("crp_property_menu")
			net.WriteString("sell")
			net.SendToServer()
			
			opened = false;
			Frame:Close()
		
		end
		
		local Title = vgui.Create("DTextEntry", panel1)
		Title:SetPos(5,40)
		Title:SetSize(175,20)
		Title:SetText(property.name)
		Title.OnEnter = function()
		
			property.name = Title:GetValue(1)
		
			sell:SetText("Sell "..property.name.." for $"..math.floor(property.cost/2))
		
			net.Start("crp_property_menu")
			net.WriteString(Title:GetValue(1))
			net.SendToServer()
		
		end
		
		--[[
		
			COOWNERS
		
		]]--
		
		local Scroll = vgui.Create( "DScrollPanel" , panel1)
		Scroll:SetSize( 187, 180 )
		Scroll:SetPos( 200, 40 )
		
		local CoList = vgui.Create( "DIconLayout", Scroll)
		CoList:SetPos(0, 0)
		CoList:SetSize(187,180)
		CoList:SetSpaceY( 0 )
		CoList:SetSpaceX( 0 )
		
		for k,v in pairs(property.coowners) do
		
			local panel = vgui.Create("DPanel", Frame)
			panel:SetPos(0,0)
			panel:SetSize(187,36)
			
			surface.SetFont("crp_HUD2")
			
			local label = vgui.Create("DLabel", panel)
			label:SetPos(40,7)
			label:SetFont("crp_HUD2")
			label:SetText(v:Nick())
			label:SizeToContents()
			
			local Avatar = vgui.Create( "AvatarImage", panel )
			Avatar:SetSize( 32, 32 )
			Avatar:SetPos( 2, 2 )
			Avatar:SetPlayer( v, 32 )
			
			CoList:Add(panel)
		
		end
	
		local addco = vgui.Create("DButton", panel1)
		addco:SetPos(5,88)
		addco:SetSize(87,30)
		addco:SetText("Add Co-owner")
		addco.DoClick = function()
		
			local Menu = DermaMenu()
			
			for k,v in pairs(player.GetAll()) do
			
				if v != LocalPlayer() and !table.HasValue(property.coowners, v) then
				
					Menu:AddOption( v:Nick() , function() 
					
						net.Start("crp_property_menu"); 
						net.WriteString("addcoowner"); 
						net.WriteEntity(v); 
						net.SendToServer();
						
						local panel = vgui.Create("DPanel", Frame)
						panel:SetPos(0,0)
						panel:SetSize(187,36)
						
						surface.SetFont("crp_HUD2")
						
						local label = vgui.Create("DLabel", panel)
						label:SetPos(40,7)
						label:SetFont("crp_HUD2")
						label:SetText(v:Nick())
						label:SizeToContents()
						
						local Avatar = vgui.Create( "AvatarImage", panel )
						Avatar:SetSize( 32, 32 )
						Avatar:SetPos( 2, 2 )
						Avatar:SetPlayer( v, 32 )
						
						table.insert(property.coowners,v)
						
						CoList:Add(panel)
					
					end )
					
				end
				
			end
			
			Menu:Open()
		
		end
		
		local remco = vgui.Create("DButton", panel1)
		remco:SetPos(93,88)
		remco:SetSize(87,30)
		remco:SetText("Remove Co-owner")
		remco.DoClick = function()
		
			local Menu = DermaMenu()
			
			for k,v in pairs(property.coowners) do
			
				if v != LocalPlayer() and table.HasValue(property.coowners,v) then
				
					Menu:AddOption( v:Nick() , function() 
					
						net.Start("crp_property_menu");
						net.WriteString("removecoowner");
						net.WriteEntity(v);
						net.SendToServer();  
						
						property.coowners[k] = nil
						
						CoList:Clear()
						
						for c,b in pairs(property.coowners) do
						
							local panel = vgui.Create("DPanel", Frame)
							panel:SetPos(0,0)
							panel:SetSize(187,36)
							
							surface.SetFont("crp_HUD2")
							
							local label = vgui.Create("DLabel", panel)
							label:SetPos(40,7)
							label:SetFont("crp_HUD2")
							label:SetText(b:Nick())
							label:SizeToContents()
							
							local Avatar = vgui.Create( "AvatarImage", panel )
							Avatar:SetSize( 32, 32 )
							Avatar:SetPos( 2, 2 )
							Avatar:SetPlayer( b, 32 )
							
							CoList:Add(panel)
						
						end
						
					end )
					
				end
				
			end
			
			Menu:Open()
		
		end
		
		if property.showshop then
		
			local shop = vgui.Create("DButton", panel1)
			shop:SetPos(5,142)
			shop:SetSize(175,30)
			
			if property.isShop then
			
				shop:SetText("Dismantle shop");
				
			else
			
				shop:SetText("Setup shop");
			
			end
			
			shop.DoClick = function()
			
				if property.isShop then
				
					property.isShop = false;
					shop:SetText("Setup shop");
				
				else
				
					property.isShop = true;
					shop:SetText("Dismantle shop");
				
				end
			
				net.Start("crp_property_menu")
				net.WriteString("shop")
				net.SendToServer()
			
			end
			
		end
		
	end

	net.Receive("crp_property_menu",function(len)
		
		local name = net.ReadString()
		local cost = net.ReadFloat()
		local coowners = net.ReadTable()
		local showshop = net.ReadBool()
		local isshop = false or net.ReadBool()
	
		property.name = name
		property.cost = cost
		property.coowners = coowners
		property.showshop = showshop
		property.isShop = isshop
		
		PropertyMenu()
	
	end)
	
	net.Receive("crp_property_forceclose", function(len)
	
		forceclose = true
	
	end)
	
end