--[[
	Jobs module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Jobs";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "Manages the jobs and employment.";

local job = {} -- "none" (unchoicable), "goverment" (goverment staff), "comercial" (dealers), "freelancer" (miscellaneous)
job["CITIZEN"] = Job("Unemployed", Color(30,200,10), "none", "citizen")

function getJobs()

	return job

end

function addJob(uid, name, col, cat, modelgroup, loadout, description)

	if job != nil and job[uid] == nil then

		local theCol = col or Color(255,255,255)
		local theCat = cat or "none"
		local theModel = modelgroup or "citizen"
		local theLoadout = loadout or {}
		local theDesc = description or [[No description available.]]

		job[uid] = Job(name, theCol, theCat, theModel, theLoadout, theDesc)
		
	end

end

if SERVER then

	function Module.spawn(ply)
	
		ply.Job = "CITIZEN"
	
		if table.Count(job[ply.Job].loadout) > 0 then
		
			for k,v in pairs(job[ply.Job].loadout) do
			
				ply:Give(v)
			
			end
		
		end

	end
	
	function BecomeJob(ply,newjob)
	
		if job[newjob] != nil then
		
			if table.Count(job[ply.Job].loadout) > 0 then
		
				for k,v in pairs(job[ply.Job].loadout) do
					
					if ply:HasWeapon(v) then
					
						ply:StripWeapon(v)
						
					end
				
				end
			
			end
		
			ply.Job = newjob
			
			if table.Count(job[newjob].loadout) > 0 then
		
				for k,v in pairs(job[newjob].loadout) do
				
					ply:Give(v)
				
				end
			
			end
			
			SetPlayermodel(ply)
			
		end
	
	end
	
	net.Receive("crp_npc_job", function(len,ply)
	
		local newjob = net.ReadString()
	
		if ply:GetEyeTrace().Hit and ply:GetEyeTrace().Entity:GetClass() == "npc_job" then
		
			if newjob == "CITIZEN" then
			
				BecomeJob(ply, newjob)
				SendHint(ply, "You've quitted your job.",2,job[ply.Job].colour)
			
			else
			
				if getJobs()[newjob].category != "none" then
		
					BecomeJob(ply, newjob)
					SendHint(ply, "You've become a "..(job[ply.Job].name).."!",1,job[ply.Job].colour)
					
				end
				
			end
		
		end
	
	end)
	
end