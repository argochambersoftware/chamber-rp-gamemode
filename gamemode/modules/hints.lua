--[[
	HUD hints module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Hint";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "Displays messages and hints.";

function Module:init()

	Chamber.log("Module Hint loaded!");
end

if SERVER then

	util.AddNetworkString("crp_hud_hint");
	
	function SendHint(ply, text, type, col)
	
		net.Start("crp_hud_hint");
			net.WriteString(text or "NULL");
			net.WriteFloat(type or 0);
			net.WriteColor(col or Color(255,255,255));
		net.Send(ply);
	
	end;

end;

if CLIENT then

	
    local messages = {}
    
    -- text, type (0 = normal, 1 = colored, 2 = blinky), col
    
    local function CreateMessage(text, type, col)
    
        local msg = {text = text, type = type, col = col, blink = 0, size = 0, time = 0, disp = false, tick = 0}
    
        table.insert(messages,1,msg)
    
    end;
    
    
    local lol = 0
    
    local function DrawMessages()
    
        surface.SetFont("crp_HUD2");
        
        local textsize = 0;
    
        for k,v in pairs(messages) do
        
            textsize = surface.GetTextSize(v.text) + 8;
        
            if v.tick < CurTime() then
            
                if !v.disp then
                
                    if v.size < textsize then
                    
                        v.size = v.size + 15
                        
                    else
                    
                        v.time = CurTime() + 4
                        v.disp = true
                    
                    end;
                    
                else
                
                    if v.time < CurTime() then
                    
                        if v.size > 0 then
                        
                            v.size = v.size - 15
                            
                        else
                        
                            table.remove(messages, k)
                        
                        end;
                        
                    else
                    
                        if v.type == 2 then
                    
                            if v.blink > 0 then
                            
                                v.blink = v.blink - 0.03
                                
                            else
                            
                                v.blink = 1
                                
                            end
                        
                        end;
                    
                    end;
                
                end;
                
                v.tick = CurTime() + 0.01;
            
            end;
			
			local colr = GetConVar("crp_hud_frame_r"):GetInt();
			local colg = GetConVar("crp_hud_frame_g"):GetInt();
			local colb = GetConVar("crp_hud_frame_b"):GetInt();
            
            if v.size >= (textsize - 15) then
            
                draw.RoundedBox(0,(ScrW()/2) - (textsize/2), ScrH() - 220 + 28*k,textsize,25,Color(colr,colg,colb,100));
            
                if v.type == 1 then
                
                    draw.RoundedBox(0,(ScrW()/2) - (textsize/2)+2, ScrH() - 218 + 28*k,textsize-4,21,Color(v.col.r,v.col.g,v.col.b,155));
                
                elseif v.type == 2 then
                
                    draw.RoundedBox(0,(ScrW()/2) - (textsize/2)+2, ScrH() - 218 + 28*k,textsize-4,21,Color(v.col.r,v.col.g,v.col.b,200*v.blink));
                
                end
                
                draw.SimpleText(v.text, "crp_HUD2", (ScrW()/2) - (textsize/2) + 2, ScrH() - 219 + 28*k, Color(255,255,255,245));
                
            else
            
                draw.RoundedBox(0,(ScrW()/2) - (textsize/2), ScrH() - 220 + 28*k,v.size,v.size*0.125,Color(colr,colg,colb,100));
                
            end;
        
        end;
    
    end;
    hook.Add("HUDPaint", "crp_hud_message", DrawMessages);
	
	net.Receive("crp_hud_hint", function(len)
	
		local txt = net.ReadString();
		local type = net.ReadFloat();
		local col = net.ReadColor();
		
		CreateMessage(txt, type, col);
	
	end);

end