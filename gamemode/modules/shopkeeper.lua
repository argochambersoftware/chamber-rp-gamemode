--[[
	Shopkeeper module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Shop system";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "The purpose of this module is to manage all the properties turned into shops and its jobs.";

function Module:think()

	addJob("SHOPKEEPER", "Shopkeeper", Color(255,180,10), "comercial", "citizen")

end