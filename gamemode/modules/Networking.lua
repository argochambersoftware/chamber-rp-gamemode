--[[
	Networking module.
	Adds a Net namespace to the Chamber namespace.
	Used to simplify the usage of some functions.

	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Networking";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "This is a simple module that holds funcions for Networking operations.";

Chamber.Net = {};

Chamber.Net.pool = function(str)
	if SERVER then
		util.AddNetworkString(str);
	end
	if CLIENT then
		Chamber.log("Client doesn't need to pool messages!");
	end
end