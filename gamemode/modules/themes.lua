--[[
	Themes - For Chamber RP

	This is a theme manager that provides themeable Derma elements and draw/surface ones.
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Themes";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "HUD and VGUI Theming system.";

if CLIENT then

	--[[
		This will register a new font to the surface call, but also returns the name of the font passed before.
		This will keep you from creating variables to hold the name twice, all in a single call.
	]]
	Chamber.newFont = function(fontName, fontData)
		surface.CreateFont(fontName, fontData);
		return fontName;
	end

	--[[
		This is the default information schema for theming.
		Acts as a blueprint and all the themes derieve from here.
	]]
	
	local colr = GetConVar("crp_hud_frame_r"):GetInt()
	local colg = GetConVar("crp_hud_frame_g"):GetInt()
	local colb = GetConVar("crp_hud_frame_b"):GetInt()
	
	local default_schema = {
		main_background = Color(colr,colg,colb,100),
		main_radius = 2,
		panel_background = Color(colr,colg,colb,50),
		panel_radius = 6,
		text_color = Color(0,0,0),
		text_font = Chamber.newFont("Default CRP Font", {
			font = "Roboto",
			size = 13,
			weight = 500,
			blursize = 0,
			scanlines = 0,
			antialias = true,
			underline = false,
			italic = false,
			strikeout = false,
			symbol = false,
			rotary = false,
			shadow = false,
			additive = false,
			outline = false,
		}),
		caption_color = Color(255,255,255),
		caption_font = Chamber.newFont("Default CRP Strong Font", {
			font = "Roboto",
			size = 14,
			weight = 800,
			blursize = 0,
			scanlines = 0,
			antialias = true,
			underline = false,
			italic = false,
			strikeout = false,
			symbol = false,
			rotary = false,
			shadow = false,
			additive = false,
			outline = false,
		}),
		title_color = Color(90, 90, 90),
		title_font = Chamber.newFont("Default CRP Title Font", {
			font = "Roboto",
			size = 21,
			weight = 500,
			blursize = 0,
			scanlines = 0,
			antialias = true,
			underline = false,
			italic = false,
			strikeout = false,
			symbol = false,
			rotary = false,
			shadow = true,
			additive = false,
			outline = false,
		}),
		hard_link_color = Color(200, 20, 20),
		hard_link_font = Chamber.newFont("Default CRP Hard Link", {
			font = "Roboto",
			size = 13,
			weight = 500,
			blursize = 0,
			scanlines = 0,
			antialias = true,
			underline = false,
			italic = true,
			strikeout = false,
			symbol = false,
			rotary = false,
			shadow = false,
			additive = false,
			outline = false,
		}),
		soft_link_color = Color(20, 20, 200),
		hover_link = Color(60, 225, 60),
		hover_element = Color(255, 225, 255),
		active_link = Color(95, 240, 60),
		active_element = Color(95, 60, 240),
		disabled_element = Color(100, 90, 90),
		disabled_link = Color(90, 70, 70),
		idle_element = Color(50, 50, 50),
		idle_link = Color(25, 25, 25)
	};


	--[[
		Theme inline class
	]]
	function Theme(name, schema)
		local o = table.Copy(default_schema);

		o.name = name;

		-- This procedure will ensure that you will inherit the default schema's data.
		for k, v in pairs(schema or {}) do
			o[k] = v;
		end

		return o;
	end

	-- Themes storage.
	Chamber.themes = {};
	Chamber.themes.DEFAULT = Theme("Default");

	-- Variables to know what theme we're using.
	local theme = Chamber.themes.DEFAULT;
	Chamber.theme = theme;

	--[[
		Sets the theme to be used.
		@param iTheme Theme
	]]
	Chamber.setTheme = function(iTheme)
		theme = iTheme;
	end

	--[[
		Gets the current theme set for rendering.
	]]
	Chamber.getTheme = function()
		return theme;
	end

	--[[----------------------------------------------------
				Now the themable vElements itesves.
	--]]----------------------------------------------------

	--[[
		Themeable DFrame
		@panel
	]]--
	do
		local PANEL = {};

		function PANEL:Paint( w, h )
			draw.RoundedBox( theme.main_radius, 0, 0, w, h, theme.main_background );
			draw.RoundedBox( theme.main_radius, 2, 2, w - 4, 26, theme.panel_background );
			
			if self.Title != nil then
			
				draw.SimpleText(self.Title,"crp_HUD1", 8,6,Color(255,255,255,255));
				
			end
			
		end

		vgui.Register( "CFrame", PANEL, "DFrame" );
	end	

	--[[
		Themeable DPanel
		@panel
	]]--
	do
		local PANEL = {};

		function PANEL:Paint( w, h )
			draw.RoundedBox( theme.panel_radius, 0, 0, w, h, theme.panel_background );
		end

		vgui.Register( "CPanel", PANEL, "DPanel" );
	end

	--[[
		Themeable DLabel
		@panel
	]]--
	do
		local PANEL = {};

		function PANEL:Init()
			self:SetColor(theme.text_color);
			self:SetFont(theme.text_font);
		end

		vgui.Register( "CLabel", PANEL, "DLabel" );
	end	

end