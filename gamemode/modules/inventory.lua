--[[
	Inventory and containers module

	This basically will contain things like widgets for the VGUI.
	The logical items are placed in classes directory, as they are.
	The referenced entities can be anything, but recommended to derieve from Storeable.
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Inventory";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "This is the inventory system. It's also the module that manages any type of container and items that are storeable.";

DROP_MSG 				= "NET_SEND_DROP_SIGNAL_CRP";
DROP_MSG_AMT 			= "SPEC_AMOUNT_DROP_SIGNAL_NET_DESC";


hook.Add("CModulesLoaded", "Chamber Inventory Load modules", function()
	-- This is called once all modules are loaded.
	if SERVER then
		Chamber.Net.pool(DROP_MSG);
		Chamber.Net.pool(DROP_MSG_AMT);
	end
end);

if SERVER then
	
	net.Receive(DROP_MSG, function(len, ply)
		local id = net.ReadInt(32);
		ply.inventory:dropStack(id);
	end);
	
	net.Receive(DROP_MSG_AMT, function(len, ply)
		local id = net.ReadInt(32);
		local qty = net.ReadInt(32);
		ply.inventory:splitStack(id, qty);
		ply.inventory:dropStack(id);
	end);
end

if CLIENT then

	print("Type: "..type(LocalPlayer()));

	--[[
		Inventory widget for the VGUI
		@panel
	]]--
	do
		local PANEL = {};
		local DEFAULT_SIZE = 16;
	
		-- Inventory 
		function PANEL:Init()
			self:SetBackgroundColor(Color(255,255,255,0));
			self:Dock(FILL);

			self.full = vgui.Create("DProgress", self);
			self.full:SetPos(10,25);
			local w,h = self:GetSize();
			self.full:SetSize(w-20, 25);
			self.full:Dock(TOP);

			self.fullLabel = vgui.Create("DLabel", self);
			self.fullLabel:SetText("0/"..DEFAULT_SIZE.." stacks used.");
			self.fullLabel:Dock(TOP);

			self.container = vgui.Create("DContainer", self);
			self.container:Dock(FILL);
			self.container.addItem = function(contnr, item)
				item:SetParent(self);
				contnr.List:Add(item);
			end
		end

		-- Initializes the panel
		function PANEL:build(cap)
			self.container:build(cap or DEFAULT_SIZE);
		end

		-- Updates the widget bar that indicates the amount used
		function PANEL:updateBar()
			self.full:SetFraction(self.container:getUsedSpace()/self.container:getCapacity());
			self.fullLabel:SetText(self.container:getUsedSpace().."/"..self.container:getCapacity().." stacks used.");
		end

		-- Adds stacks to the widget (Inherits from container)
		function PANEL:addStacks(stacks)
			self.container:addStacks(stacks);
			self:updateBar();
		end

		-- Adds a single stack to the internal container.
		function PANEL:addStack(stack, SUID)
			self.container:addStack(stack, SUID);
			self:updateBar();
		end

		-- Removes the item (Ensures that the panel will be updated)
		function PANEL:removeItem(item)
			self.container:removeItem(item);
			self:updateBar();
		end

		--[[
			Load serial data from the network.
			This will load the data that has been serialized from a container and setn throught the network.
			@param Table Stacks Serialized
		]]
		function PANEL:loadSerialData(stackdata)
			table.sort(stackdata);
			for k, v in pairs(stackdata) do
				self:addStack( Stack( Chamber.getItem(v["id"]), v["qty"] ), k );
			end
		end

		vgui.Register( "DInventory", PANEL, "CPanel" );
	end




end
