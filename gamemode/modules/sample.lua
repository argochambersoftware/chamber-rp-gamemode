--[[
	Sample module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Sample";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "The purpose of this module is just to serve as a placeholder and example.";

function Module:init()

	Chamber.log("Module SAMPLE loaded!");
end