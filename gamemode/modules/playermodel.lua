--[[
	Playermodel module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Playermodel";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "It manages the playermodel of the player.";

function Module:init()

	Chamber.log("Module PLAYERMODEL loaded!");
end

if SERVER then

	local function canChange(ply)
	
		return true
	
	end
	
	local function isMale()
	
		if string.sub(ply.Playermodel,1,4) == "male" then
		
			return true
			
		else
		
			return false
		
		end
	
	end
	
	function SetPlayermodel(ply)
	
		if getJobs()[ply.Job].model == "citizen" then
		
			ply:SetModel("models/player/group01/"..(ply.Playermodel)..".mdl")
		
		elseif getJobs()[ply.Job].model == "rebel" then
		
			ply:SetModel("models/player/group03/"..(ply.Playermodel)..".mdl")
		
		elseif getJobs()[ply.Job].model == "metrocop" then
		
			ply:SetModel("models/player/Police.mdl")
		
		elseif getJobs()[ply.Job].model == "president" then
		
			ply:SetModel("models/player/breen.mdl")
		
		end
	
	end
	
	function Module.spawn(ply)
	
		ply.Playermodel = "male_07"
		
		if ply.Job == nil then
		
			ply:SetModel("models/player/group01/"..(ply.Playermodel)..".mdl")
			
		else
		
			SetPlayermodel(ply)
		
		end
	
	end

end