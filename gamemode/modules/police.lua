--[[
	Police module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Police Job";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "The purpose of this module is to manage the police force features.";

function Module:think()

	addJob("POLICE", "Police Officer", Color(0,40,255), "goverment", "metrocop", {"weapon_mad_usp"})

end