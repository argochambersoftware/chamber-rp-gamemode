--[[
	HUD module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "HUD";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "The gamemode's heads up display.";

function Module:init()

	Chamber.log("Module HUD loaded!");
end

if CLIENT then

	
    surface.CreateFont( "crp_HUD1", {
    font = "Roboto",
    size = 18,
    weight = 500,
    blursize = 0,
    scanlines = 0,
    antialias = true,
    underline = false,
    italic = false,
    strikeout = false,
    symbol = false,
    rotary = false,
    shadow = false,
    additive = false,
    outline = false
    })
    
    surface.CreateFont( "crp_HUD2", {
    font = "Roboto",
    size = 24,
    weight = 1000,
    blursize = 0,
    scanlines = 0,
    antialias = true,
    underline = false,
    italic = false,
    strikeout = false,
    symbol = false,
    rotary = false,
    shadow = false,
    additive = false,
    outline = false
    })
    
    surface.CreateFont( "crp_HUD3", {
    font = "Roboto",
    size = 42,
    weight = 1000,
    blursize = 0,
    scanlines = 0,
    antialias = true,
    underline = false,
    italic = false,
    strikeout = false,
    symbol = false,
    rotary = false,
    shadow = false,
    additive = false,
    outline = false
    })
    
    local faderate = 15
    
	--[[
	
		CONVARS
	
	]]--
	
	local hud_frame_r = CreateClientConVar( "crp_hud_frame_r", 255, true, false, "HUD Frame Color: Red amount" );
	local hud_frame_g = CreateClientConVar( "crp_hud_frame_g", 255, true, false, "HUD Frame Color: Green amount" );
	local hud_frame_b = CreateClientConVar( "crp_hud_frame_b", 255, true, false, "HUD Frame Color: Blue amount" );
	
    local color1 = Color(hud_frame_r:GetInt(),hud_frame_g:GetInt(),hud_frame_b:GetInt(),100);
    local color2 = Color(hud_frame_r:GetInt(),hud_frame_g:GetInt(),hud_frame_b:GetInt(),50);
    
    local textcol1 = Color(255,255,255,125);
    local textcol2 = Color(255,255,255,245);
    
    /* PIECES */
    
    local function DrawBar(x,y,var,max,text,col)
    
        draw.RoundedBox(0, x,y, 200, 25, color1);
        draw.RoundedBox(0, x + 25,y + 2, 2, 21, color1);
        draw.RoundedBox(0, x + 28,y + 2, 170, 21, color2);
        draw.RoundedBox(0, x + 28,y + 2, 170*(var/max), 21, Color(col.r,col.g,col.b,155));
        
        draw.SimpleText(text, "crp_HUD1", x + 30, y + 8,textcol1);
        draw.SimpleText(var, "crp_HUD2", x + 160, y + 1,textcol2);
    
    end
    
    /* ELEMENTS */
    
    local function DrawTime(x,y)
    
        draw.RoundedBox(0, x,y, 75,25,color1)
        draw.SimpleText("00"..":".."00", "crp_HUD2", x + 35, y + 1,textcol2,1)
    
    end
    
    local lasthp = 0
    local hptime = 0
    local hpsize = 0
    local hptick = 0
    local hpallowed = false
    
    local cachehp = 100
    local cachetime = 0
    
    local lowtick = 0
    local col = Color(255,255,255)
    
    local hlowtime = 0
    local hlowsize = 0
    local hlowtick = 0
    local hlowallowed = false
    
    local hlowcol = 0
    
    local function DrawHealth(x,y)
    
        local hp = LocalPlayer():Health()
    
        if hp > 50 then
        
            col = Color(235*((50-(hp-50))/50),235,0)
        
        elseif hp > 10 and hp <= 50 then
        
            col = Color(235,235*((hp-10)/40),0)
            
        else
        
            if lowtick < CurTime() then
            
                if col.r == 200 then
                
                   col = Color(255,0,0)
                
                else
                
                    col = Color(200,0,0)
                
                end
            
                lowtick = CurTime() + 0.5
            
            end
        
        end
        
        if (lasthp != hp) then
        
            if hpsize < 200 then
    
                if hptick < CurTime() then
                
                    hpsize = hpsize + faderate
                    hptick = CurTime() + 0.01
                
                end
                
                draw.RoundedBox(0, x,y, hpsize, hpsize*0.125, color1)
                
            else
            
                hptime = CurTime() + 4
                hpallowed = true
                cachetime = CurTime() + 1
                lasthp = hp
    
            end       
    
        else
        
            if hptime <= CurTime() and (hp > 50 or hp <= 0) then
            
                if hpallowed == true then
                
                    hpallowed = false
                
                end
            
                if hpsize > 0 then
    
                    if hptick < CurTime() then
                    
                        hpsize = hpsize - faderate
                        hptick = CurTime() + 0.01
                    
                    end
                    
                    draw.RoundedBox(0, x,y, hpsize, hpsize*0.125, color1)
                    
                end
            
            end
            
        end
        
        if hpallowed == true then
        
            if cachehp > hp then
            
                if hptick < CurTime() and cachetime < CurTime() then
                
                    cachehp = cachehp - 1
                    hptick = CurTime() + 0.01
                
                end
                
                DrawBar(x,y, hp,100, "Vital signs", col)
                draw.RoundedBox(0, x + 28 + (170*((hp)/100)),y + 2, 170*((cachehp - hp)/100), 21, Color(255,0,0,180))
            
            elseif cachehp < hp then
            
                cachehp = hp
                
                DrawBar(x,y, hp,100, "Vital signs", col)
            
            else
        
                DrawBar(x,y, hp,100, "Vital signs", col)
                
            end
            
            surface.SetMaterial(Material("gui/pulse.png"))
            surface.SetDrawColor(Color(255,255,255,255))
            surface.DrawTexturedRect( x + 5, y + 1, 16,23)
        
        end
        
        
        /*  --  LOW HEALTH MESSAGE  -- */
        
        if hp <= 25 and hp > 0 then
        
            if hlowsize < 200 then
    
                if hlowtick < CurTime() then
                
                    hlowsize = hlowsize + faderate
                    hlowtick = CurTime() + 0.01
                
                end
                
                draw.RoundedBox(0, (ScrW()/2) - 100,ScrH() - 230, hlowsize, hlowsize*0.125, color1)
    
            else        
                
                draw.RoundedBox(0, (ScrW()/2) - 100,ScrH() - 230, 200, 25, color1)
                
                if hlowtick < CurTime() then
                
                    if hlowcol == 0 then
                    
                        hlowcol = 235
                        
                    else
                    
                        hlowcol = 0
                    
                    end
                    
                    hlowtick = CurTime() + 0.01 + (0.29*(hp/25))
                    
                end
                
                draw.RoundedBox(0, (ScrW()/2) - 98,ScrH() - 228, 196, 21, Color(235,hlowcol,hlowcol,155))
                
                draw.SimpleText("Vital signs critical", "crp_HUD2", (ScrW()/2), ScrH() - 228, textcol2,1)
                        
            end
            
        else
        
            if hlowsize > 0 then
            
                if hlowtick < CurTime() or (hlowtick - CurTime() > 0.01) then
                
                    hlowsize = hlowsize - faderate
                    hlowtick = CurTime() + 0.01
                
                end
                
                draw.RoundedBox(0, (ScrW()/2) - 100,ScrH() - 230, hlowsize, hlowsize*0.125, color1)
            
            end
            
        end
    
    end
    
    local lastwep = ""
    local weptime = 0
    local wepsize = 0
    local weptick = 0
    local wepallowed = false
    
    local lastammo = 0
        
    local function DrawAmmo(x,y)
        
        if LocalPlayer():KeyDown(IN_ATTACK) then
        
            lastwep = ""
        
        end
        
        if (lastwep != LocalPlayer():GetActiveWeapon():GetClass()) then
        
            if wepsize < 220 then
    
                if weptick < CurTime() then
                
                    wepsize = wepsize + faderate
                    weptick = CurTime() + 0.01
                
                end
                
                if LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType() > -1 then
                
                    draw.RoundedBox(0, x + 220 - wepsize,y + 68 - wepsize*(68/220), wepsize, wepsize*(68/220), color1)
                
                else
                
                    draw.RoundedBox(0, x + 220 - (wepsize*0.6),y + 68 - (wepsize*0.6)*(68/132), wepsize*0.6, (wepsize*0.6)*(68/132), color1)
                
                end
                
            else
            
                weptime = CurTime() + 4
                wepallowed = true
                lastwep = LocalPlayer():GetActiveWeapon():GetClass()
    
            end       
    
        else
        
            if weptime <= CurTime() then
            
                if wepallowed == true then
                
                    wepallowed = false
                
                end
            
                if wepsize > 0 then
    
                    if weptick < CurTime() then
                    
                        wepsize = wepsize - faderate
                        weptick = CurTime() + 0.01
                    
                    end
                    
                    if LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType() > -1 then
                
                        draw.RoundedBox(0, x + 220 - wepsize,y + 68 - wepsize*(68/220), wepsize, wepsize*(68/220), color1)
                    
                    else
                
                        draw.RoundedBox(0, x + 220 - (wepsize*0.6),y + 68 - (wepsize*0.6)*(68/132), wepsize*0.6, (wepsize*0.6)*(68/132), color1)
                
                    end
                    
                end
            
            end
            
        end
        
        if wepallowed == true then
        
            if weptick < CurTime() then
            
                if lastammo < LocalPlayer():GetAmmoCount(LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType()) then
                
                    lastammo = lastammo + 1
                
                elseif lastammo > LocalPlayer():GetAmmoCount(LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType()) then
                
                    lastammo = lastammo - 1
                
                end
               
                weptick = CurTime() + 0.01
             
            end
        
            if LocalPlayer():GetActiveWeapon().WepSelectIcon != nil then
            
                draw.SimpleText("Weapon", "crp_HUD1", x + 92, y + 49,Color(255,255,255,135))
            
                if LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType() > -1 then
                
                    draw.RoundedBox(0,x,y,220,68,color1)
                    draw.RoundedBox(0,x + 87,y + 2,2,64,color1)
                    
                else
                
                    draw.RoundedBox(0,x + 88,y,132,68,color1)
                
                end
                
                if LocalPlayer():GetActiveWeapon():Clip1() > -1 then
                
                    draw.RoundedBox(0,x + 90,y + 2 + (64 - 64*(LocalPlayer():GetActiveWeapon():Clip1()/LocalPlayer():GetActiveWeapon().Primary.ClipSize)),128,64*(LocalPlayer():GetActiveWeapon():Clip1()/LocalPlayer():GetActiveWeapon().Primary.ClipSize),Color(255,255,255,50))
                
                end
         
                surface.SetTexture(LocalPlayer():GetActiveWeapon().WepSelectIcon)
                surface.SetDrawColor(Color(255,255,255,255))
                surface.DrawTexturedRect(x + 90, y + 2, 128, 64)
            
                draw.SimpleText(LocalPlayer():GetActiveWeapon():GetPrintName(), "crp_HUD2", x + 216, y + 2,Color(255,255,255,75),2)
                
                if LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType() > -1 then
                    
                    if LocalPlayer():GetActiveWeapon():Clip1() == -1 then
                    
                        draw.SimpleText(LocalPlayer():GetAmmoCount(LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType()), "crp_HUD3", x + 44, y + 14,Color(255,255,255,245),1)
                        
                    else
                    
                        if (LocalPlayer():GetActiveWeapon():Clip1()/LocalPlayer():GetActiveWeapon().Primary.ClipSize) <= 0.25 then
                        
                            draw.SimpleText(LocalPlayer():GetActiveWeapon():Clip1(), "crp_HUD3", x + 44, y + 4,Color(255,0,0,245),1)
                        
                        else
                        
                            draw.SimpleText(LocalPlayer():GetActiveWeapon():Clip1(), "crp_HUD3", x + 44, y + 4,Color(255,255,255,245),1)
                        
                        end
                        
                        draw.SimpleText(lastammo, "crp_HUD2", x + 43, y + 40,Color(255,255,255,245),1)
                    end
                    
                end
                
            end
            
        end
        
    end
    
    local lastmoney = 0
    local moneytime = 0
    local moneysize = 0
    local moneytick = 0
    local moneyallowed = false
    
    local cachemoney = 0
    
    local function DrawMoney(x,y)
    
        local money = LocalPlayer():GetNWFloat("money")
    
        if (lastmoney != money) then
        
            if moneysize < 200 then
    
                if moneytick < CurTime() then
                
                    moneysize = moneysize + faderate
                    moneytick = CurTime() + 0.01
                
                end
                
                draw.RoundedBox(0, x + 200 - moneysize,y, moneysize, moneysize*0.125, color1)
                
            else
            
                moneytime = CurTime() + 4
                moneyallowed = true
                lastmoney = money
    
            end       
    
        else
        
            if moneytime <= CurTime() then
            
                if moneyallowed == true then
                
                    moneyallowed = false
                
                end
            
                if moneysize > 0 then
    
                    if moneytick < CurTime() then
                    
                        moneysize = moneysize - faderate
                        moneytick = CurTime() + 0.01
                    
                    end
                    
                    draw.RoundedBox(0, x + 200 - moneysize,y, moneysize, moneysize*0.125, color1)
                    
                end
            
            end
            
        end
        
        local money = LocalPlayer():GetNWFloat("money")
        local add = 0
        local diff = 0
		local mult = 1
        
        if moneyallowed == true then
        
            if moneytick < CurTime() then
            
                if cachemoney < money then
                
					diff = (money - cachemoney)
					mult = 1
					
				elseif cachemoney > money then
				
					diff = (cachemoney - money)
					mult = -1
					
				end
				
				if cachemoney != money then
				
                    if diff > 1000000 then
                    
                        add = 1000000
                        
                    elseif diff > 100000 and diff <= 1000000 then
                    
                        add = 100000
                    
                    elseif diff > 10000 and diff <= 100000 then
                    
                        add = 10000
                    
                    elseif diff > 1000 and diff <= 10000 then
                    
                        add = 1000
                    
                    elseif diff > 100 and diff <= 1000 then
                    
                        add = 100
                    
                    elseif diff > 10 and diff <= 100 then
                    
                        add = 10
                    
                    else
                    
                        add = 1
                    
                    end
					
				end
				
                cachemoney = cachemoney + add*mult
                
                moneytick = CurTime() + 0.03
            
            end
        
            draw.RoundedBox(0, x,y, 200, 25, color1)
            draw.RoundedBox(0, x + 173,y + 2, 2, 21, color1)
        
            draw.SimpleText("$","crp_HUD2", x + 181, y + 1, Color(255,255,255,245))
            draw.SimpleText("Money", "crp_HUD1", x + 4, y + 8,Color(255,255,255,125))
            draw.SimpleText(cachemoney,"crp_HUD2", x + 167, y + 1, Color(255,255,255,245),2)
        
        end

    end
    
    local lastwanted = -1
    local wantedtime = 0
    local wantedsize = 0
    local wantedtick = 0
    local wantedallowed = false
    
    local wanted = 0
    local status = 0
    local wcache = 21
    
    local function DrawWanted(x,y)
    
        if (lastwanted != wanted) and lastwanted <= 0 then
        
            if wantedsize < 200 then
    
                if wantedtick < CurTime() then
                
                    wantedsize = wantedsize + faderate
                    wantedtick = CurTime() + 0.01
                
                end
                
                draw.RoundedBox(0, x + 200 - wantedsize,y, wantedsize, wantedsize*0.125, color1)
                
            else
            
                wantedtime = CurTime() + 4
                wantedallowed = true
                wcache = 23
                lastwanted = wanted
    
            end       
    
        else
        
            if wantedtime <= CurTime() and wanted == 0 then
            
                if wantedallowed == true then
                
                    wantedallowed = false
                
                end
            
                if wantedsize > 0 then
    
                    if wantedtick < CurTime() then
                    
                        wantedsize = wantedsize - faderate
                        wantedtick = CurTime() + 0.01
                    
                    end
                    
                    draw.RoundedBox(0, x + 200 - wantedsize,y, wantedsize, wantedsize*0.125, color1)
                    
                end
            
            end
            
        end
        
        local separation = 16
    
        if wantedallowed then
            
            if status == 0 then
            
                if wanted == 0 then
                
                    draw.RoundedBox(0, x,y, 200, 25, color1)
                    draw.SimpleText("Felony", "crp_HUD1", x + 4, y + 8,Color(255,255,255,125))
                    draw.SimpleText("CLEAN", "crp_HUD2", x + 100, y + 2,Color(255,255,255,245),1)
            
                elseif wanted > 0 then
                
                    if wcache > 0 then
                    
                        if wantedtick < CurTime() then
                        
                            wcache = wcache - 1
                            wantedtick = CurTime() + 0.05
                        
                        end
                    
                    end
                
                    draw.RoundedBox(0, x,y, 200, 25, Color(255,255,255,100))
                    
                    surface.SetMaterial(Material("gui/star.png"))
                    
                    draw.SimpleText("Felony", "crp_HUD1", x + 4, y + 8,Color(255,255,255,125))
                    
                    if wanted == 0 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 12, y + 4, 16,16)
                    
                    if wanted <= 1 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 28 + separation, y + 4, 16,16)
                    
                    if wanted <= 2 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 44 + separation*2, y + 4, 16,16)
                    
                    if wanted <= 3 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 60 + separation*3, y + 4, 16,16)
                    
                    if wanted <= 4 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 76 + separation*4, y + 4, 16,16)
                    
                    if wanted <= 5 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 92 + separation*5, y + 4, 16,16)
                    
                    if wcache > 0 then
                    
                        draw.RoundedBox(0, x+2,y+2 + 21 - wcache, 196, wcache, Color(235,50,0,215))
                        draw.SimpleText("WANTED", "crp_HUD2", x + 100, y + 2,Color(255,255,255,245),1)
                        
                    end
                    
                end
                
            elseif status == 1 then
            
                draw.RoundedBox(0, x,y, 200, 25, Color(255,255,255,100))
                draw.SimpleText("Felony", "crp_HUD1", x + 4, y + 8,Color(255,255,255,125))
                
                    surface.SetMaterial(Material("gui/star.png"))
                
                     if wanted == 0 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 12, y + 4, 16,16)
                    
                    if wanted <= 1 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 28 + separation, y + 4, 16,16)
                    
                    if wanted <= 2 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 44 + separation*2, y + 4, 16,16)
                    
                    if wanted <= 3 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 60 + separation*3, y + 4, 16,16)
                    
                    if wanted <= 4 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 76 + separation*4, y + 4, 16,16)
                    
                    if wanted <= 5 then
                        surface.SetDrawColor(Color(0,0,0,100))
                    else
                        surface.SetDrawColor(Color(255,255,255,245))
                    end
                    surface.DrawTexturedRect(x + 92 + separation*5, y + 4, 16,16)
                
                draw.RoundedBox(0, x+2,y+2, 196, 21, Color(0,125,235,215))
                draw.SimpleText("EVADING", "crp_HUD2", x + 100, y + 2,Color(255,255,255,245),1)
            
            end
            
        end
        
    end
    
    /* The FUNCTION */
    
    local function Reset()
    
        lasthp = -1
        lastwep = ""
        lastmoney = -1
        
        if wanted == 0 then
        
            lastwanted = -1
            
        end
    
    end
    
    /* The HUD */
        
    local function HUD()
    
		--[[
		
			COLOR REFRESH
		
		]]--
		
			color1 = Color(hud_frame_r:GetInt(),hud_frame_g:GetInt(),hud_frame_b:GetInt(),100)
			color2 = Color(hud_frame_r:GetInt(),hud_frame_g:GetInt(),hud_frame_b:GetInt(),50)
		
		--[[
		
			HUD FUNCTIONS
		
		]]--
        
        DrawTime(20,70)
        DrawHealth(20,100)
        
        if LocalPlayer():GetActiveWeapon() != NULL then
        
            DrawAmmo(ScrW() - 240, ScrH() - 130)
            
        end
        
        DrawMoney(ScrW() - 240,100)
        DrawWanted(ScrW() - 240,70)
        
        if LocalPlayer():KeyDown(IN_SCORE) then
        
            Reset()
        
        end
        
        --DrawBar(20,130, LocalPlayer():Armor(),100, "Kevlar vest", Color(0,165,235)) -- Debug
    
    end
    hook.Add("HUDPaint", "crp_hud", HUD)
    
    local tohide = { -- This is a table where the keys are the HUD items to hide
    ["CHudHealth"] = true,
    ["CHudBattery"] = true,
    ["CHudAmmo"] = true,
    ["CHudSecondaryAmmo"] = true
    }
    
    local function HUDShouldDraw(name)
    
        if (tohide[name]) then
        
           return false;
        
        end
        
    end
    hook.Add("HUDShouldDraw", "crp_HUDhide", HUDShouldDraw)


end