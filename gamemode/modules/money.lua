--[[
	Sample module
	Chamber RP
	---
	Argochamber Interactive 2016
]]

------ MODULE HEADER ------
Module.name 			= "Money";
Module.author 			= "Argochamber Interactive";
Module.version 			= "1.0";
Module.desc 			= "Money module, to store your $money$";

if SERVER then

	function Module.spawn(ply)

		ply:SetNWFloat("money", 100+ply:GetNWFloat("money"));

	end

end