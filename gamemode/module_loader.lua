--[[
	Gamemode Module load script.
	Chamber RP
	---
	Argochamber Interactive 2016
]]
AddCSLuaFile();

-- Module list
local Modules = {};

-- Scan the modules!
local files = file.Find("gamemodes/chamberrp/gamemode/modules/*.lua", "MOD");

for k, v in pairs(files) do

	Chamber.log("Loading module "..v);
	Module = {};

	include("modules/"..v);

	if not Module.name then Chamber.log("Modules must have a name!", Chamber.log.Level.Warn, "Module Loader"); end

	Chamber.log("Loaded module "..v..((Module.name and " as "..Module.name) or " with no name!"));
	table.insert(Modules, Module);

	Module = {};

end

-- Now load the module handler. The startup script will be called on init.
Chamber.modulesInit = function()

	-- Do hook the functions, if defined.
	for k, v in pairs(Modules) do
		local MODULE_PREFIX = k..(v.Name or "NULLNAME");
		-- Custom module calls.
		if v.load then
			hook.Add("Chamber.LoadModule", MODULE_PREFIX.."_LOAD_H", v.loaded);
		end
		if v.unload then
			hook.Add("Chamber.UnloadModule", MODULE_PREFIX.."_UNLOAD_H", v.unload);
		end
		-- Default GMod hooks
		if v.think then
			hook.Add("Think", MODULE_PREFIX.."_THINK_H", v.think);
		end
		if v.tick then
			hook.Add("Tick", MODULE_PREFIX.."_TICK_H", v.tick);
		end
		if v.init then
			hook.Add("Initialize", MODULE_PREFIX.."_INIT_H", v.init);
		end
		if v.spawn and SERVER then
			hook.Add( "PlayerSpawn", MODULE_PREFIX.."_SPAWN_H", v.spawn );
		end
		if v.initpostent and SERVER then
			hook.Add( "InitPostEntity", MODULE_PREFIX.."_POSTENT_H", v.initpostent );
		end
		Modules[k].hooksPrefix = MODULE_PREFIX; -- To identify the modules.
	end

end