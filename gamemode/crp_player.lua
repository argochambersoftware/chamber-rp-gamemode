--[[
	Player baseclass definition
	This will be used as a superclass for the other player classes.

	Chamber RP
	---
	Argochamber Interactive 2016
]]
AddCSLuaFile();


--
-- Name: PLAYER:Spawn
-- Desc: Called serverside only when the player spawns
-- Arg1:
-- Ret1:
--
local function PlySpawn(ply)
	
	local melons = Stack(Chamber.getItem(1), 64);
	local barrels = Stack(Chamber.getItem(2), 64);
	local doors = Stack(Chamber.getItem(3), 64);
	local plates = Stack(Chamber.getItem(4), 64);


	ply.inventory:addStack(melons);
	ply.inventory:addStack(barrels);
	ply.inventory:addStack(doors);
	ply.inventory:addStack(plates);
	
end
hook.Add( "PlayerSpawn", "CRPPlayerRegisterAllSpawn", PlySpawn );


function GM:OnSpawnMenuOpen()
	Chamber.getContainer();
end

function GM:OnSpawnMenuClose()
	if InventoryFrame then
		InventoryFrame:Close();
	end
end


if CLIENT then
	local MARGIN = 20;
	net.Receive(Chamber.containers.NET_CONTAINER_REQUEST,function(len)
		local t = net.ReadTable();
		InventoryFrame = vgui.Create("CFrame");
		InventoryFrame:SetPos(MARGIN, MARGIN);
		InventoryFrame:SetSize(ScrW() - MARGIN*2, ScrH() - MARGIN*2);
		InventoryFrame:SetTitle( "Inventory" );
		InventoryFrame:SetDraggable( false );
		InventoryFrame:MakePopup();
		InventoryFrame:ShowCloseButton( false );

		--Gen the inventory
		local cont = vgui.Create("DInventory", InventoryFrame);
		cont:build(32);
		cont:loadSerialData(t);

	end);
end

function GM:PlayerDisconnected( ply )
	PrintMessage( HUD_PRINTTALK, ply:Name().. " has left the server." );
end

local function OnSpawn(ply)
	ply.inventory = Container( ply, 32 );
	Chamber.log("First Spawn for "..ply:GetName());
end

hook.Add( "PlayerInitialSpawn", "CRPPlayerRegisterSpawn", OnSpawn );