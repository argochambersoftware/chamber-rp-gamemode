--[[
	Gamemode Class loader. Gets classes ready to be used.
	Chamber RP
	---
	Argochamber Interactive 2016
]]
AddCSLuaFile();

-- Scan the classpath!
local files = file.Find("gamemodes/chamberrp/gamemode/classes/*.lua", "MOD");

for k, v in pairs(files) do

	include("classes/"..v);

end