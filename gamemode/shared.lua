--[[
	Gamemode Shared super globals Initialization
	Chamber RP
	---
	Argochamber Interactive 2016
]]
AddCSLuaFile();
DeriveGamemode("sandbox");

-- Server Settings and data
GM.Name 			= "Chamber RP";
GM.Author			= "Argochamber Interactive 2016";
GM.Email 			= "N/A";
GM.Website			= "http://argochamber.com/"
GM.Version 			= "1.0.0";

-- Main configuration, is private, read only. Want to change config on the run? Use Env Vars.
local CONF = {};
CONF.verbose		= true; -- Print all logs?
CONF.level 			= {"fine", "info", "config", "warn", "severe", "error", "failure"}; -- Log level names.
CONF.fireLevel 		= 4; -- At what log level will print
CONF.defaultLogLevel= 1;

--[[
	Chamber Namespace.
	This will contain some shared (Or not) calls, like setVar(); and others.
]]
Chamber = {};
C = Chamber; -- Delegator

-- Gets a configuration variable.
Chamber.getConfig = function(var)
	return CONF[var] or "Null";
end

-- Log level binds.
Chamber.log = {};
Chamber.log.Level = {
	Fine = 1,
	Info = 2,
	Config = 3,
	Warn = 4,
	Severe = 5,
	Error = 6,
	Failure = 7
};

-- Logs a message.
setmetatable(Chamber.log, {
	__call = function(self, msg, level, classname, shouldNotify)
		if not msg then error("A log must have a message!!"); end

		level = level or CONF.defaultLogLevel;
		shouldNotify = shouldNotify or false;

		if CONF.verbose == true or level >= CONF.fireLevel or shouldNotify == true then

			print("{"..((GM and GM.Name) or "Default Gamemode").."}["..CONF.level[level].."]: "..msg..((classname and " (at "..classname..")") or ""));

		end

	end
});

-- Environment vars and sync vars.
local EnvVars = {};
local SyncVars = {};

-- Gets a variable, only for the side that is being called!
Chamber.setVar = function(varname, value)
	EnvVars[varname] = value;
end

-- Same as avobe, but GET.
Chamber.getVar = function(varname)
	return EnvVars[varname];
end

local setVar = "Chamber.SYNC_VAR_M_SET";
if SERVER then util.AddNetworkString(setVar); end -- The idientifier for the message.

-- This will send a message to the server or the client (Depending on the side that is being called) to set a sync var.
Chamber.setSync = function(varname, value)
	SyncVars[varname] = value;
	net.Start(setVar);
		net.WriteTable({[varname] = value});
	if SERVER then net.Broadcast(); end
	if CLIENT then net.SendToServer(); end
end

-- We supose that the variables are synchronized when set on the other side.
Chamber.getSync = function(varname)
	return SyncVars[varname];
end

-- This the update handler
net.Receive(setVar, function(len, ply)
	local tbl = net.ReadTable();
	if SERVER then
		if team.GetName( ply:Team() ) == "admin" then
			for k, v in pairs(tbl) do
				SyncVars[k] = v;
			end
		else
			Chamber.log("Player not allowed to set environment variables on the server.");
		end
	end
	if CLIENT then
		for k, v in pairs(tbl) do
			SyncVars[k] = v;
		end
	end
end);

-- This is the item registry handler.
local itemsRegistry = {};

Chamber.registerItem = function(item)
	table.insert(itemsRegistry, item);
	Chamber.log("Registered a new item ["..#itemsRegistry.."] named "..item.getName());
	return #itemsRegistry;
end

Chamber.getItem = function(id)
	return itemsRegistry[id];
end

----------- CHAMBER BASIC STARTUP -----------

Chamber.log("Chamber RP Startup.");
include("class_loader.lua");
include("module_loader.lua");

Chamber.modulesInit(); -- Load up the hooks for each module.

include("game/hooks.lua");

-- This will be called at the end, meaning that all that had to been loaded is done.
hook.Run("Chamber.LoadModule");
hook.Run("CModulesLoaded");

--[[
	Default base items.
	This is the table of items that come by default with the "Engine" Gamemode.
	To add more, register them in modules.
]]
local test = Item("test");
local plate = Item("Metal Plate", "models/props_phx/construct/metal_plate1.mdl");
local barrel = Item("Barrel", "models/props_borealis/bluebarrel001.mdl");
local armDoor = Item("Armored Door", "models/props_borealis/borealis_door001a.mdl");

-- # Load the Player class # --
include("crp_player.lua");
